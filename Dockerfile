FROM paco0512/wildfly8.2.0:rootVersion1
RUN /opt/jboss/wildfly/bin/add-user.sh mia julia  --silent
ADD docker/wildfly/modules/ /opt/jboss/wildfly/modules/
ADD build/libs/Projektmanagement-Tool.war /opt/jboss/wildfly/standalone/deployments/
ADD docker/wildfly/standalone/configuration/ /opt/jboss/wildfly/standalone/configuration/
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement","0.0.0.0"]