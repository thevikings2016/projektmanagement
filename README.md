# README #

## Projektmanagement-Tool ##

# Test #
Employee: `4480` PW: `a`
Leader: `1481` PW: `a`
Manager: `1842` PW: `a`

# Technik #
* UI: `JSF` --> Primefaces (http://primefaces.org/)
* Backend: `JavaEE`
* Database: `Postgres V9.4`
* Application Server: `Wildfly 8.2.0 FINAL`


### Wichtig ###
* Jira-URL: http://31.172.93.251:7779
* Jenkins-URL: http://31.172.93.251:8080
* Docker-Version: `1.10.3`  
* Jira Credentials= `vorname.nachname` Passwort (default) = `123456`
* Server Credentials= User=`dolly` Pass=`12345678`
* Gradle und JDK in Path aufnehmen: (http://bryanlor.com/blog/gradle-tutorial-how-install-gradle-windows)

### Setup Requirements ###

* ###Für die die Eclipse benutzen: mind. Eclipse `Mars` mit folgenden Add-Ons: `JBoss Tools` + `Buildship` (Gradle-Plugin)###
* Docker [Download] (https://www.docker.com/products/docker-toolbox)
* Sourcetree (GUI für Git) [Download] (https://www.sourcetreeapp.com/)
* Git (falls noch nicht installiert) [Downloadlink und Einführung in Git] (https://rogerdudler.github.io/git-guide/index.de.html)

### Conventions ###

* Variablennamen und Kommentare sind ausschließlich auf Englisch
* Pro Feature einen Branch von Develop erstellen, nach Implementierung Featurebranch in DEVELOP mergen
* Der Master Branch wird von Zeit zu Zeit nachgezogen und soll als laufbereites Back-Up dienen
* Credentials und Release-Tags haben folgende Namensconvention: `V-Major.Minor-Name`, wobei Name eine weibliche Schauspielerin ist

### Grundlagen Tutorials ###

* Ganz ganz schneller Überblick über jsf und Primefaces, dauert 20min und ist ultra nützlich wenn ihr noch nie etwas damit gemacht habt. Mir hats ziemlich geholfen:
http://de.slideshare.net/martyhall/a-whirlwind-tour-of-jsf-22-programming
* Großes Primfaces Basic Tutorial:
http://www.coreservlets.com/JSF-Tutorial/primefaces/index.html


### Projektmanagement Vorlagen ###
* http://www.pm-handbuch.com/pm-vorlagen/
* Unsere verbindliche Vorlage: