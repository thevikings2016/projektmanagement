package util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import domain.ProjectEntity;
import domain.ProjectIdeaEntity;

public class EmptyObjectProviderTest {

        @Test
        public void provideEmptyProjectInstance() {
                ProjectEntity emptyInstance = EmptyObjectProvider.getEmptyInstance(ProjectEntity.class);
                boolean instance = (emptyInstance instanceof ProjectEntity);
                assertEquals(true, instance);
        }

        @Test
        public void provideEmptyProjectIdeaInstance() {
                ProjectIdeaEntity emptyInstance = EmptyObjectProvider.getEmptyInstance(ProjectIdeaEntity.class);
                boolean instance = (emptyInstance instanceof ProjectIdeaEntity);
                assertEquals(true, instance);
        }

}
