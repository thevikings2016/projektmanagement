package presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import domain.Configuration;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import domain.ProjectIdeaEntity;
import enums.Extent;
import repository.ProjectIdeaRepository;
import repository.ProjectLogRepository;
import repository.ProjectRepository;
import testSuiteUtils.IntegrationTestArchiveBuilder;
import util.ProjectRater;

@RunWith(Arquillian.class)
public class CreateProjectPresenterIT extends IntegrationTestArchiveBuilder {

        private static Logger LOGGER = LoggerFactory.getLogger(CreateProjectPresenterIT.class);

        // Store Info because every Test has its own Instance
        private static ProjectEntity projectTestInstance;
        private static ProjectIdeaEntity projectIdeaTestInstance;

        private CreateProjectPresenter createProjectPresenter = new CreateProjectPresenter();

        private ProjectIdeaEntity projectIdea;

        @Inject
        private ProjectLogRepository projectLogRepository;

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private ProjectIdeaRepository projectideaRepository;

        @Inject
        private ProjectRater projectRater;

        private User user = new User();

        @Deployment
        public static WebArchive createDeployment() {
                return createPreparedWebArchive("CreateProjectRepository");
        }

        @Before
        public void init() {
                user.setUserId(4880);
                createProjectPresenter.setProjectLogRepository(projectLogRepository);
                createProjectPresenter.setProjectRepository(projectRepository);
                createProjectPresenter.setProjectideaRepository(projectideaRepository);
                createProjectPresenter.setUser(user);
                projectIdea = createProjectIdea();
        }

        @Test
        @InSequence(value = 5)
        public void initializeDatabase() {
                projectideaRepository.saveIdea(projectIdea);
                projectIdeaTestInstance = projectIdea;
        }

        @Test
        @InSequence(value = 10)
        public void createProjectFromIdea() {
                createProjectPresenter.setProjectIdea(projectIdeaTestInstance);
                createProjectPresenter.createProjectfromIdea(projectIdeaTestInstance.getId().toString());
                projectTestInstance = createProjectPresenter.getProject();
                assertEquals(projectIdea.getName(), projectTestInstance.getName());
        }

        @Test
        @InSequence(value = 20)
        public void saveProject() {
                Configuration config = projectRater.getConfig();
                ProjectIdeaEntity projectIdeaEntity = projectideaRepository.findIdeaByID(projectIdeaTestInstance.getId());
                fillProjectWithData(projectTestInstance, projectIdeaEntity);
                projectTestInstance.setRating(projectRater.getProjectRating(projectTestInstance, config));
                createProjectPresenter.setProjectIdea(projectIdeaEntity);
                createProjectPresenter.setProject(projectTestInstance);
                createProjectPresenter.setProjectRater(projectRater);
                createProjectPresenter.saveProjectAndWriteLog();
                ProjectEntity projectEntity = projectRepository.findProjectByID(projectTestInstance.getId());
                assertNotNull(projectEntity);
                ProjectIdeaEntity ideaEntity = projectideaRepository.findIdeaByID(projectIdeaTestInstance.getId());
                assertNull(ideaEntity);
                Integer numberOfDeletedLogs = projectLogRepository.removeAllLogsByProjectID(projectTestInstance.getId());
                assertTrue(numberOfDeletedLogs == 1);
        }

        @Test
        @InSequence(value = 100)
        public void tearDown() {
                projectRepository.delete(projectTestInstance);
                assertNull(projectRepository.findProjectByID(projectTestInstance.getId()));
        }

        private void fillProjectWithData(ProjectEntity project, ProjectIdeaEntity projectIdeaEntity) {
                project.setBenefit(projectIdeaEntity.getBenefit());
                project.setObjective(projectIdeaEntity.getObjective());
                project.setCurrentSituation(projectIdeaEntity.getCurrentSituation());
                project.setName(projectIdeaEntity.getName());
                project.setActualCosts(100);
                project.setCostUnit(projectIdeaEntity.getDepartment());
                project.setLocation(projectIdeaEntity.getLocation());
                project.setAvgProceedsPerYear(20);
                project.setCustomer(projectIdeaEntity.getCreator());
                project.setCustomerStrategy(10);
                project.setEnddate(new Date());
                project.setExtCosts(10);
                project.setExtent(1);
                project.setImageGain(10);
                project.setInnovationStrategy(10);
                project.setIntCosts(10);
                project.setManager(projectIdeaEntity.getCreator());
                project.setNameRecognitionIncrease(10);
                project.setNpv(10);
                project.setPricingStrategy(10);
                project.setProgress(2);
                project.setRisk(1);
                project.setStartdate(new Date());
                project.setTimingStrategy(10);
                project.setUrgency(2);
                project.setUseDuration(1);
                project.setWorkflowState(3);
                project.setRoi(20);
        }

        private ProjectIdeaEntity createProjectIdea() {
                DepartmentEntity department = new DepartmentEntity();
                department.setId(10);
                User creator = new User();
                creator.setUserId(4880);
                creator.setDepartment(department);
                LocationEntity location = new LocationEntity();
                location.setId(1);
                ProjectIdeaEntity projectIdea = new ProjectIdeaEntity(creator, department, location);
                projectIdea.setName("Ich komme vom Integrationstest");
                projectIdea.setCreatedate(new Date());
                projectIdea.setWorkflowState(1);
                projectIdea.setCurrentSituation("Aktuell ist alles doof und ich habe kein Bock mehr");
                projectIdea.setObjective("Ein kaltes Bier nach Feierabend w�rde meine Laune erheblich verbessern");
                projectIdea.setBenefit("Ein gl�cklicher Mitarbeiter mit einem fetten Grinsen");
                projectIdea.setExtent(Extent.Standortprojekt.getValue());
                return projectIdea;
        }

}
