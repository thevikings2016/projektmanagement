package config;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import enums.Role;
import testSuiteUtils.AbstractFacesContextTest;
import testSuiteUtils.UserService;

public class LoginBeanTest extends AbstractFacesContextTest {

        @InjectMocks
        private LoginBean loginBean;

        private User user_LEADER;
        private User user_EMPLOYEE;;

        @Before
        public void init() {
                MockitoAnnotations.initMocks(this);
                initMockedFacesContext(true);
                user_EMPLOYEE = UserService.getUserByRole(Role.EMPLOYEE);
                user_LEADER = UserService.getUserByRole(Role.LEADER);
        }

        @Test
        public void loginTest() {
                Boolean succeeded = loginBean.loginWithUserSucceeded(user_LEADER);
                assertEquals(true, succeeded);
        }

        @Test
        public void proceedRoleMappingTest() {
                loginBean.proceedRoleMapping(user_LEADER);
                loginBean.proceedRoleMapping(user_EMPLOYEE);
                assertEquals(Role.LEADER, user_LEADER.getRole());
                assertEquals(Role.EMPLOYEE, user_EMPLOYEE.getRole());
        }

}
