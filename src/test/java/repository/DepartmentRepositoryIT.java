package repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import domain.DepartmentEntity;
import repository.DepartmentRepository;
import testSuiteUtils.IntegrationTestArchiveBuilder;

@RunWith(Arquillian.class)
public class DepartmentRepositoryIT extends IntegrationTestArchiveBuilder {

        private final Integer departmentIDfromIT = 7;

        @Inject
        private DepartmentRepository departmentRepository;

        @Deployment
        public static WebArchive createDeployment() {
                return createPreparedWebArchive("DepartmentRepository");
        }

        @Test
        public void testFindAllDepartments() {
                assertNotNull(departmentRepository);
                assertTrue(departmentRepository.findAll().size() > 0);
        }

        @Test
        public void testFindSpecificDepartmentByID() {
                DepartmentEntity departmentEntity = departmentRepository.findById(departmentIDfromIT);
                assertEquals("IT", departmentEntity.getFullname().trim());
        }

}
