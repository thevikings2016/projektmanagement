package repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectIdeaEntity;
import enums.Extent;
import enums.State;
import testSuiteUtils.IntegrationTestArchiveBuilder;

@RunWith(Arquillian.class)
public class ProjectIdeaRepositoryIT extends IntegrationTestArchiveBuilder {

        private ProjectIdeaEntity projectIdea;

        private static Integer testID;

        @Inject
        private ProjectIdeaRepository ideaRepository;

        @Deployment
        public static WebArchive createDeployment() {
                return createPreparedWebArchive("CreateIdeaRepository");
        }

        @Before
        public void initTest() {
                projectIdea = createProjectIdea();
        }

        @Test
        @InSequence(value = 5)
        public void getNextId() {
                testID = ideaRepository.getNextId();
                assertTrue(testID > 0);
        }

        @Test
        @InSequence(value = 10)
        public void saveIdea() {
                projectIdea.setWorkflowState(State.requested.getValue());
                projectIdea.setCreatedate(new Date());
                ideaRepository.saveIdea(projectIdea);
                ProjectIdeaEntity ideaEntity = ideaRepository.findIdeaByID(testID);
                assertNotNull(ideaEntity);
        }

        @Test
        @InSequence(value = 20)
        public void updateIdea() {
                projectIdea.setWorkflowState(State.putaside.getValue());
                projectIdea.setId(testID);
                ideaRepository.updateIdea(projectIdea);
                ProjectIdeaEntity ideaEntity = ideaRepository.findIdeaByID(testID);
                assertEquals(State.putaside.getValue(), ideaEntity.getWorkflowState());
        }

        @Test
        @InSequence(value = 200)
        public void tearDown() {
                projectIdea.setId(testID);
                ideaRepository.deleteIdea(projectIdea);
                ProjectIdeaEntity ideaEntity = ideaRepository.findIdeaByID(testID);
                assertNull(ideaEntity);
        }

        private ProjectIdeaEntity createProjectIdea() {
                DepartmentEntity department = new DepartmentEntity();
                department.setId(10);
                User creator = new User();
                creator.setUserId(4880);
                creator.setDepartment(department);
                LocationEntity location = new LocationEntity();
                location.setId(1);
                ProjectIdeaEntity projectIdea = new ProjectIdeaEntity(creator, department, location);
                projectIdea.setName("Ich komme vom Integrationstest");
                projectIdea.setCurrentSituation("Aktuell ist alles doof und ich habe kein Bock mehr");
                projectIdea.setObjective("Ein kaltes Bier nach Feierabend w�rde meine Laune erheblich verbessern");
                projectIdea.setBenefit("Ein gl�cklicher Mitarbeiter mit einem fetten Grinsen");
                projectIdea.setExtent(Extent.Standortprojekt.getValue());
                return projectIdea;
        }
}
