package repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import enums.Extent;
import testSuiteUtils.IntegrationTestArchiveBuilder;

@Ignore
@RunWith(Arquillian.class)
public class ProjectRepositoryIT extends IntegrationTestArchiveBuilder {

        private ProjectEntity project;
        private static Integer testID;

        @Inject
        private ProjectRepository projectRepository;

        @Deployment
        public static WebArchive createDeployment() {
                return createPreparedWebArchive("CreateIdeaPresenter");
        }

        @Before
        public void initTest() {
                project = createProject();
        }

        @Test
        @InSequence(value = 5)
        public void getNextId() {
                testID = projectRepository.getNextId();
                assertTrue(testID > 0);
        }

        @Test
        @InSequence(value = 10)
        public void saveProject() {
                projectRepository.saveNewProject(project);
                ProjectEntity ideaEntity = projectRepository.findProjectByID(testID);
                assertNotNull(ideaEntity);
        }

        @Test
        @InSequence(value = 200)
        public void tearDown() {
                projectRepository.delete(project);
        }

        private ProjectEntity createProject() {
                DepartmentEntity department = new DepartmentEntity();
                department.setId(10);
                User creator = new User();
                creator.setUserId(4880);
                creator.setDepartment(department);
                LocationEntity location = new LocationEntity();
                location.setId(1);
                ProjectEntity projectIdea = new ProjectEntity(creator, creator, location, department);
                projectIdea.setName("Ich komme vom Integrationstest");
                projectIdea.setCurrentSituation("Aktuell ist alles doof und ich habe kein Bock mehr");
                projectIdea.setObjective("Ein kaltes Bier nach Feierabend w�rde meine Laune erheblich verbessern");
                projectIdea.setBenefit("Ein gl�cklicher Mitarbeiter mit einem fetten Grinsen");
                projectIdea.setExtent(Extent.Standortprojekt.getValue());
                return project;
        }
}
