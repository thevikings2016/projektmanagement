package repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import domain.LocationEntity;
import testSuiteUtils.IntegrationTestArchiveBuilder;

@RunWith(Arquillian.class)
public class LocationRepositoryIT extends IntegrationTestArchiveBuilder {

        private static final Integer locationID = 3;

        @Inject
        private LocationRepository locationRepository;

        @Deployment
        public static WebArchive createDeployment() {
                return createPreparedWebArchive("LocationRepository");
        }

        @Test
        public void testFindAllLocations() {
                assertTrue(locationRepository.findAll().size() > 0);
        }

        @Test
        public void testFindSpecificLocationByID() {
                LocationEntity locationEntity = locationRepository.findById(locationID);
                assertEquals("Hamburg", locationEntity.getCity().trim());
        }

        @Test
        public void testFindByName() {
                List<LocationEntity> findbyName = locationRepository.findbyName("Freiburg");
                assertEquals("Freiburg im Breisgau", findbyName.get(0).getCity().trim());
        }

}
