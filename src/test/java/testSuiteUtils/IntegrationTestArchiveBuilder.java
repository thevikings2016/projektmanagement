package testSuiteUtils;

import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

public class IntegrationTestArchiveBuilder {

        private static final String DEPLOYMENT_NAME = "proj-%s-test.war";

        public static WebArchive createPreparedWebArchive(String testset) {
                WebArchive webArchive = ShrinkWrap.create(WebArchive.class, String.format(DEPLOYMENT_NAME, testset));
                webArchive.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
                File[] file = Maven.resolver().resolve("org.slf4j:slf4j-api:1.7.21").withoutTransitivity().asFile();
                webArchive.addAsLibraries(file);
                // include META-INF folder
                webArchive.addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml");
                webArchive.addClass(IntegrationTestArchiveBuilder.class);
                webArchive.addPackage("domain");
                webArchive.addPackage("enums");
                webArchive.addPackage("util");
                webArchive.addPackage("presenter");
                webArchive.addPackage("repository");
                webArchive.addPackage("config");
                webArchive.addPackage("integration");
                System.out.println(String.format("Created Web-Archive with content:\r\n%s\r\n", webArchive.toString(true)));

                return webArchive;
        }

}
