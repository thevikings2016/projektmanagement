package testSuiteUtils;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.mockito.Mockito;

public abstract class AbstractFacesContextTest {

        private static FacesContext ctx;

        public static void initMockedFacesContext(Boolean mockSession) {
                initFacesContext();
                if (mockSession) {
                        initMockedSession();
                }

        }

        private static void initFacesContext() {
                ctx = ContextMocker.mockFacesContext();
                ExternalContext externalContext = Mockito.mock(ExternalContext.class);
                Mockito.when(ctx.getExternalContext()).thenReturn(externalContext);
        }

        private static void initMockedSession() {
                HttpSession session = Mockito.mock(HttpSession.class);
                Mockito.when(ctx.getExternalContext().getSession(false)).thenReturn(session);
        }

}
