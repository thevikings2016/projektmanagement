package testSuiteUtils;

import org.jboss.weld.exceptions.IllegalArgumentException;

import config.User;
import domain.DepartmentEntity;
import enums.Role;

public abstract class UserService {

        private static DepartmentEntity department = new DepartmentEntity();

        private static void init() {
                department.setSupervisor(1481);
                department.setId(0);
        }

        public static User getUserByRole(Role role) {
                init();
                switch (role) {
                case EMPLOYEE:
                        return getEmployee();
                case LEADER:
                        return getLeader();
                case MANAGER:
                        return getManager();
                default:
                        throw new IllegalArgumentException("Undefined Role " + role);
                }
        }

        private static User getEmployee() {
                User user = new User();
                user.setUserId(4880);
                user.setDepartment(department);
                return user;
        }

        private static User getLeader() {
                User user = new User();
                user.setUserId(1481);
                department.setId(2);
                user.setDepartment(department);
                return user;
        }

        private static User getManager() {
                User user = new User();
                user.setUserId(1481);
                user.setDepartment(department);
                return user;
        }
}
