package comparator;

import java.util.Comparator;

import domain.DepartmentEntity;

public class DepartmentComparator implements Comparator<DepartmentEntity> {

        @Override
        public int compare(DepartmentEntity o1, DepartmentEntity o2) {
                return o1.getFullname().compareTo(o2.getFullname());
        }
}
