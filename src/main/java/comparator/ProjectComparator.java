package comparator;

import java.util.Comparator;

import domain.ProjectEntity;

/**
 * @author pascal.euhus
 *
 *         Comparator f�r die Priorisierung um Projekte miteinander vergleichen
 *         zu k�nnen anhand der Dringlichkeit und der Bewertung
 *
 */
public class ProjectComparator implements Comparator<ProjectEntity> {

        @Override
        public int compare(ProjectEntity o1, ProjectEntity o2) {
                int urgencyResult = o2.getUrgency().compareTo(o1.getUrgency());
                if (urgencyResult != 0) {
                        return urgencyResult;
                }
                return o2.getRating().compareTo(o1.getRating());
        }

}
