package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.ProjectEntity;
import util.ResultExtractHelper;

@Stateless
public class ProjectRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(ProjectRepository.class);

        @Inject
        private EntityManager em;

        public List<ProjectEntity> findAll() {
                LOGGER.info("Getting all Project Instances");
                TypedQuery<ProjectEntity> query = em.createQuery("SELECT x FROM ProjectEntity x", ProjectEntity.class);
                return query.getResultList();
        }

        public List<ProjectEntity> findAllOrdered() {
                LOGGER.info("Getting all Project Instances with Workflowstate 1,2,4");
                TypedQuery<ProjectEntity> query = em.createQuery(
                                "SELECT x FROM ProjectEntity x WHERE workflow_state = '1' OR workflow_state = '2' OR workflow_state = '4' ORDER BY urgency DESC, rating DESC",
                                ProjectEntity.class);
                return query.getResultList();
        }

        public Integer getNextId() {
                TypedQuery<Integer> query = em.createQuery("SELECT MAX(x.id) FROM ProjectEntity x", Integer.class);
                Integer maxId = ResultExtractHelper.getSingleResultOrNull(query);
                return maxId + 1;
        }

        public ProjectEntity findProjectByID(Integer id) {
                TypedQuery<ProjectEntity> query = em.createQuery("SELECT x FROM ProjectEntity x WHERE x.id = :id", ProjectEntity.class);
                query.setParameter("id", id);
                return ResultExtractHelper.getSingleResultOrNull(query);
        }

        public List<ProjectEntity> findAllByDepartment(Integer departmentId) {
                LOGGER.info("Getting all Project Instances of Department {}", departmentId);
                TypedQuery<ProjectEntity> query = em.createQuery("SELECT x FROM ProjectEntity x WHERE x.costUnit.id = :departmentId",
                                ProjectEntity.class);
                query.setParameter("departmentId", departmentId);
                return query.getResultList();
        }

        @Transactional
        public void saveNewProject(ProjectEntity project) {
                LOGGER.info("Saving new Project {}", project);
                project.setId(getNextId());
                project.setProgress(0);
                project.setWorkflowState(1);
                project.setActualCosts(0);
                em.merge(project);
        }

        public void updateProject(ProjectEntity project) {
                em.merge(project);
                em.joinTransaction();
        }

        public void delete(ProjectEntity project) {
                em.remove(em.contains(project) ? project : em.merge(project));
        }
}