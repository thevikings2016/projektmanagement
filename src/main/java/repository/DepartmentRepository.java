package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.DepartmentEntity;
import util.ResultExtractHelper;

@Stateless
public class DepartmentRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentRepository.class);

        @Inject
        private EntityManager entityManager;

        public List<DepartmentEntity> findAll() {
                TypedQuery<DepartmentEntity> query = entityManager.createQuery("SELECT x FROM DepartmentEntity x", DepartmentEntity.class);
                return query.getResultList();

        }

        public DepartmentEntity findById(Integer id) {
                TypedQuery<DepartmentEntity> query = entityManager.createQuery("SELECT x FROM DepartmentEntity x WHERE id=:id",
                                DepartmentEntity.class);
                query.setParameter("id", id);
                return ResultExtractHelper.getSingleResultOrNull(query);
        }

}
