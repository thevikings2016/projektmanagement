package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.DepartmentEntity;
import domain.ProjectLogEntity;
import util.ResultExtractHelper;

@Stateless
public class ProjectLogRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(ProjectLogRepository.class);

        @Inject
        private EntityManager entityManager;

        public List<ProjectLogEntity> findAll() {
                LOGGER.info("Exec Query");
                TypedQuery<ProjectLogEntity> query = entityManager.createQuery("SELECT x FROM ProjectLogEntity x", ProjectLogEntity.class);
                return query.getResultList();
        }
        
        public List<ProjectLogEntity> findByProjId(Integer proj_id) {
    		TypedQuery<ProjectLogEntity> query = entityManager.createQuery("SELECT x FROM ProjectLogEntity x WHERE proj_id=:proj_id",
    				ProjectLogEntity.class);
    		query.setParameter("proj_id", proj_id);
    		return query.getResultList();
    	}

        public Integer getNextId() {
                TypedQuery<Integer> query = entityManager.createQuery("SELECT MAX(x.id) FROM ProjectLogEntity x", Integer.class);
                Integer maxId = ResultExtractHelper.getSingleResultOrNull(query);
                return maxId + 1;
        }

        @Transactional
        public void addProjectLog(ProjectLogEntity projectLog) {
                LOGGER.info("Exec Query");
                projectLog.setId(getNextId());
                entityManager.persist(projectLog);
        }

        public Integer removeAllLogsByProjectID(Integer id) {
                Query query = entityManager.createQuery("Delete from ProjectLogEntity x where x.projectId = :id");
                query.setParameter("id", id);
                return query.executeUpdate();
        }

}
