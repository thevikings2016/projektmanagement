package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import util.ResultExtractHelper;

@Stateless
public class UserRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(UserRepository.class);

        @Inject
        EntityManager em;

        public User findUser(Integer loginId, String loginPass) {
                User user = null;
                LOGGER.debug("Trying to find User with Credentials [ID: {}]", loginId);
                TypedQuery<User> query = em.createQuery("Select x from User x where x.userId=:id and x.password=:password", User.class);
                query.setParameter("id", loginId);
                query.setParameter("password", loginPass);
                user = ResultExtractHelper.getSingleResultOrNull(query);
                return user;
        }

        public List<User> findAll() {
                LOGGER.info("Exec Query");
                TypedQuery<User> query = em.createQuery("SELECT x FROM User x", User.class);
                return query.getResultList();
        }

        public List<User> findbyName(String name) {
                LOGGER.info("Trying to find Users by Name {}", name);
                TypedQuery<User> query = em.createQuery("SELECT x FROM User x where LOWER(CONCAT(x.firstname,x.lastname)) LIKE :name",
                                User.class);
                name = name.replaceAll("\\s+", "");
                query.setParameter("name", "%" + name.toLowerCase() + "%");
                return query.getResultList();
        }

}
