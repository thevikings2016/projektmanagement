package repository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import domain.Configuration;
import util.ResultExtractHelper;

@Stateless
public class ConfigurationRepository {

        @Inject
        private EntityManager em;

        public Configuration loadConfiguration() {
                TypedQuery<Configuration> query = em.createQuery("SELECT x FROM Configuration x", Configuration.class);
                return ResultExtractHelper.getSingleResultOrNull(query);
        }

        @Transactional
        public void saveConfiguration(Configuration config) {
                em.merge(config);
        }

}
