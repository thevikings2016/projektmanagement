package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.LocationEntity;
import util.ResultExtractHelper;

@Stateless
public class LocationRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(LocationRepository.class);

        @Inject
        private EntityManager entityManager;

        public List<LocationEntity> findAll() {
                LOGGER.info("Exec Query");
                TypedQuery<LocationEntity> query = entityManager.createQuery("SELECT x FROM LocationEntity x", LocationEntity.class);
                return query.getResultList();

        }

        public LocationEntity findById(Integer id) {
                TypedQuery<LocationEntity> query = entityManager.createQuery("SELECT x FROM LocationEntity x WHERE id=:id",
                                LocationEntity.class);
                query.setParameter("id", id);
                return ResultExtractHelper.getSingleResultOrNull(query);
        }

        public List<LocationEntity> findbyName(String name) {
                LOGGER.info("Trying to find Users by Name {}", name);
                TypedQuery<LocationEntity> query = entityManager.createQuery("SELECT x FROM LocationEntity x where x.city LIKE :name",
                                LocationEntity.class);
                query.setParameter("name", "%" + name + "%");
                return query.getResultList();
        }
}
