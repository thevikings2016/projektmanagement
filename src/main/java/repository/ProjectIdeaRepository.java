package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import domain.ProjectIdeaEntity;
import util.ResultExtractHelper;

@Stateless
public class ProjectIdeaRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(ProjectIdeaRepository.class);

        @Inject
        private EntityManager em;

        public List<ProjectIdeaEntity> findAll() {
                LOGGER.info("Exec Query");
                TypedQuery<ProjectIdeaEntity> query = em.createQuery("SELECT x FROM ProjectIdeaEntity x", ProjectIdeaEntity.class);
                return query.getResultList();
        }

        public ProjectIdeaEntity findIdeaByID(Integer id) {
                TypedQuery<ProjectIdeaEntity> query = em.createQuery("Select x from ProjectIdeaEntity x where x.id = :id",
                                ProjectIdeaEntity.class);
                query.setParameter("id", id);
                return ResultExtractHelper.getSingleResultOrNull(query);
        }

        public Integer getNextId() {
                TypedQuery<Integer> query = em.createQuery("SELECT MAX(x.id) FROM ProjectIdeaEntity x", Integer.class);
                Integer maxId = ResultExtractHelper.getSingleResultOrNull(query);
                return maxId + 1;
        }

        public List<ProjectIdeaEntity> findAllByDepartmentAndCreator(User user) {
                LOGGER.info("Exec Query");
                TypedQuery<ProjectIdeaEntity> query = em.createQuery(
                                "SELECT x FROM ProjectIdeaEntity x WHERE x.department.id = :departmentId or x.creator.id = :creatorId",
                                ProjectIdeaEntity.class);
                query.setParameter("departmentId", user.getDepartment().getId());
                query.setParameter("creatorId", user.getUserId());
                return query.getResultList();
        }

        public List<ProjectIdeaEntity> findAllByCreator(Integer creatorId) {
                LOGGER.info("Exec Query");
                TypedQuery<ProjectIdeaEntity> query = em.createQuery("SELECT x FROM ProjectIdeaEntity x WHERE x.creator.id = :creatorId",
                                ProjectIdeaEntity.class);
                query.setParameter("creatorId", creatorId);
                return query.getResultList();
        }

        @Transactional
        public void saveIdea(ProjectIdeaEntity projectIdea) {
                projectIdea.setId(getNextId());
                LOGGER.debug("Saving idea: " + projectIdea);
                em.merge(projectIdea);
        }

        @Transactional
        public void updateIdea(ProjectIdeaEntity idea) {
                em.merge(idea);
        }

        public void deleteIdea(ProjectIdeaEntity idea) {
                em.remove(em.contains(idea) ? idea : em.merge(idea));
        }
}
