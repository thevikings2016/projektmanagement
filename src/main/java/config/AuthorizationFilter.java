package config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import enums.Role;

/**
 * @author pascal.euhus
 *
 *         Der Filter regelt die Zugriffsberechtigungen in der Application. F�r
 *         jeden Seitenaufruf wird doFilter aufgerufen und gepr�ft ob eine
 *         Berechtigungsverletzung vorliegt
 *
 */
@WebFilter(filterName = "accessFilter", urlPatterns = { "/secure/*" })
public class AuthorizationFilter implements Filter {

        Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
        }

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
                HttpServletRequest req = (HttpServletRequest) request;
                HttpServletResponse res = (HttpServletResponse) response;
                HttpSession session = (HttpSession) req.getSession();
                if (session.getAttribute("user") == null) {
                        res.sendRedirect(req.getContextPath() + "/");
                } else if (((User) session.getAttribute("user")).getRole().equals(Role.EMPLOYEE)
                                && req.getRequestURI().substring(req.getContextPath().length()).contains("admin")) {
                        res.sendRedirect(req.getContextPath() + "/error.jsf");
                } else if (((User) session.getAttribute("user")).getRole().equals(Role.LEADER)
                                && (req.getRequestURI().contains("priority") || req.getRequestURI().contains("settings"))) {
                        res.sendRedirect(req.getContextPath() + "/error.jsf");
                } else {
                        chain.doFilter(request, response);
                }

        }

        /*
         * (non-Javadoc)
         * 
         * @see javax.servlet.Filter#destroy()
         * 
         * Wird �ber die LoginBean gemanaged
         * 
         */
        @Override
        public void destroy() {
        }

}
