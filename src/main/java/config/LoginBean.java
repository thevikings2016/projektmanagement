package config;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import enums.Role;
import repository.UserRepository;

/**
 * @author pascal.euhus
 * 
 *         LoginBean regelt die Rollenvergabe und User-Erzeugung
 *
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {

        private static final long serialVersionUID = 4498095989032479113L;

        Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);

        private Integer loginId;
        private String loginPass;

        @Inject
        private UserRepository userRepository;

        private User user;

        public String login() {
                this.user = userRepository.findUser(loginId, loginPass);
                if (loginWithUserSucceeded(user)) {
                        return "secure/home?faces-redirect=true";
                } else {
                        LOGGER.info("Invalid Credentials");
                        setWarnMessage("Achtung", "Bitte korrekte Anmeldedaten eingeben!");
                        return "Index";
                }
        }

        protected Boolean loginWithUserSucceeded(User user) {
                Boolean sucess = false;
                if (user != null) {
                        proceedRoleMapping(user);
                        LOGGER.info("Login successfull, User has Role {}", user.getRole());
                        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                        session.setAttribute("user", user);
                        sucess = true;
                }
                return sucess;

        }

        protected void setWarnMessage(String title, String message) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, title, message));
        }

        public void logout() throws IOException {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.invalidateSession();
                ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        }

        protected void proceedRoleMapping(User user) {
                Integer supervisorID = user.getDepartment().getSupervisor();
                if (user.getDepartment().getId().equals(1)) {
                        user.setUserRole(Role.MANAGER);
                } else if (user.getUserId().equals(supervisorID)) {
                        user.setUserRole(Role.LEADER);
                } else {
                        user.setUserRole(Role.EMPLOYEE);
                }
        }

        public boolean isLoggedIn() {
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                if (session.getAttribute("user") != null) {
                        return true;
                } else {
                        return false;
                }
        }

        public User getUser() {
                return user;
        }

        public String getLoginPass() {
                return loginPass;
        }

        public void setLoginPass(String loginPass) {
                this.loginPass = loginPass;
        }

        public Integer getLoginId() {
                return loginId;
        }

        public void setLoginId(Integer loginId) {
                this.loginId = loginId;
        }

        protected void setUserRepository(UserRepository userRepository) {
                this.userRepository = userRepository;
        }
}
