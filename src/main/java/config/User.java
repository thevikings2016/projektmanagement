package config;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import domain.DepartmentEntity;
import enums.Role;

@Entity
@Table(name = "employee")
public class User implements Serializable {

        private static final long serialVersionUID = 6546569944603820745L;

        @Id
        @Column(name = "id")
        private Integer userId;
        private String firstname;
        private String lastname;
        private String email;
        private String password;

        @Transient
        private Role role;

        @Transient
        private String fullname;

        @ManyToOne
        @JoinColumn(name = "department")
        private DepartmentEntity department;

        protected void setUserRole(Role userRole) {
                this.role = userRole;
        }

        public Integer getUserId() {
                return userId;
        }

        public void setUserId(Integer userId) {
                this.userId = userId;
        }

        public String getFirstname() {
                return firstname;
        }

        public void setFirstname(String firstname) {
                this.firstname = firstname;
        }

        public String getLastname() {
                return lastname;
        }

        public void setLastname(String lastname) {
                this.lastname = lastname;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public Role getRole() {
                return role;
        }

        public DepartmentEntity getDepartment() {
                return department;
        }

        public void setDepartment(DepartmentEntity department) {
                this.department = department;
        }

        public String getFullname() {
                if (getFirstname() != null) {
                        this.fullname = getFirstname() + ' ' + getLastname();
                } else {
                        this.fullname = "";
                }
                return fullname;
        }

        public void setFullname(String fullname) {
                this.fullname = fullname;
        }

        @Override
        public String toString() {
                return "User [userId=" + userId + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", password="
                                + password + ", role=" + role + ", department=" + department + "]";
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((email == null) ? 0 : email.hashCode());
                result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
                result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
                result = prime * result + ((password == null) ? 0 : password.hashCode());
                result = prime * result + ((userId == null) ? 0 : userId.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                User other = (User) obj;
                if (email == null) {
                        if (other.email != null)
                                return false;
                } else if (!email.equals(other.email))
                        return false;
                if (firstname == null) {
                        if (other.firstname != null)
                                return false;
                } else if (!firstname.equals(other.firstname))
                        return false;
                if (lastname == null) {
                        if (other.lastname != null)
                                return false;
                } else if (!lastname.equals(other.lastname))
                        return false;
                if (password == null) {
                        if (other.password != null)
                                return false;
                } else if (!password.equals(other.password))
                        return false;
                if (userId == null) {
                        if (other.userId != null)
                                return false;
                } else if (!userId.equals(other.userId))
                        return false;
                return true;
        }

}
