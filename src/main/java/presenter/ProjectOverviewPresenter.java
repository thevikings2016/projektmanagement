package presenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comparator.DepartmentComparator;
import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import enums.Extent;
import enums.Role;
import enums.State;
import enums.Urgency;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectRepository;
import util.UserProvider;

/**
 * @author pascal.euhus
 *
 *         Dieser Presenter ist f�r die project-overview zust�ndig. Je nach
 *         Berechtigung werden die Daten f�r die Tabelle aus der DB geholt.
 *
 */
@Named
@ViewScoped
public class ProjectOverviewPresenter implements Serializable {

        private static final long serialVersionUID = -7543663248331119869L;

        public static final Logger LOGGER = LoggerFactory.getLogger(ProjectOverviewPresenter.class);

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private LocationRepository locationRepository;

        @Inject
        private UserProvider userProvider;

        @Inject
        private DepartmentRepository departmentRepository;

        private List<ProjectEntity> projects = new ArrayList<>();

        private List<LocationEntity> locations = new ArrayList<>();

        private List<DepartmentEntity> departments = new ArrayList<>();

        private DepartmentComparator departmentComparator = new DepartmentComparator();

        private Boolean disabled = true;

        private List<Boolean> showColumn;

        private ProjectEntity selectedProject;

        private User user;

        @PostConstruct
        private void init() {
                this.user = userProvider.getUser();
                if (user.getRole() == Role.LEADER) {
                        this.projects = projectRepository.findAllByDepartment(user.getDepartment().getId());
                } else {
                        this.projects = projectRepository.findAll();
                }
                this.locations = locationRepository.findAll();
                this.departments = departmentRepository.findAll();
                this.showColumn = Arrays.asList(true, true, false, false, true, false, true, false, true, false, true, true, false, false,
                                false, false, false, false, false, false, false, false, false, false, false, false, true, false, true,
                                true);
        }

        public Urgency[] getPrio() {
                return Urgency.values();
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public State[] getState() {
                return State.values();
        }

        public void onRowClickSelect(final SelectEvent event) {
                disabled = false;
        }

        public void onToggle(ToggleEvent e) {
                showColumn.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
        }

        public Integer sortByDepartment(Object o1, Object o2) {
                if (o1 instanceof DepartmentEntity && o2 instanceof DepartmentEntity) {
                        return departmentComparator.compare((DepartmentEntity) o1, (DepartmentEntity) o2);
                } else {
                        throw new IllegalArgumentException();
                }

        }

        public List<ProjectEntity> getProjects() {
                return projects;
        }

        public void setProjects(List<ProjectEntity> projects) {
                this.projects = projects;
        }

        public ProjectEntity getSelectedProject() {
                return selectedProject;
        }

        public void setSelectedProject(ProjectEntity selectedProject) {
                this.selectedProject = selectedProject;
        }

        public List<LocationEntity> getLocations() {
                return locations;
        }

        public List<DepartmentEntity> getDepartments() {
                return departments;
        }

        public void setDepartments(List<DepartmentEntity> departments) {
                this.departments = departments;
        }

        public List<Boolean> getShowColumn() {
                return showColumn;
        }

        public Boolean getDisabled() {
                return disabled;
        }

        public void setDisabled(Boolean disabled) {
                this.disabled = disabled;
        }

}