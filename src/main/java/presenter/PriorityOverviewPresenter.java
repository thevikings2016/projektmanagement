package presenter;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comparator.ProjectComparator;
import config.User;
import domain.Configuration;
import domain.ProjectEntity;
import domain.ProjectLogEntity;
import enums.Extent;
import enums.State;
import enums.Urgency;
import repository.ConfigurationRepository;
import repository.ProjectLogRepository;
import repository.ProjectRepository;
import util.UserProvider;

/**
 * @author pascal.euhus
 * 
 *         Dieser Presenter ist f�r die priority-overview. Hier ist die Gesamte
 *         Logik zur Priorisierung und zur Initialisierung der Picklist.
 *
 */
@Named
@ViewScoped
public class PriorityOverviewPresenter implements Serializable {

        private static final long serialVersionUID = -7543663248331119869L;

        public static final Logger LOGGER = LoggerFactory.getLogger(PriorityOverviewPresenter.class);

        public static final Map<Integer, ProjectEntity> mapForConverter = new HashMap<>();

        private final List<ProjectEntity> projectsForAYear = new LinkedList<>();

        private final List<ProjectEntity> availableProjects = new LinkedList<>();

        private final Map<Integer, Integer> workflowStates = new HashMap<>();

        private final Map<Integer, ProjectEntity> putasideProjects = new HashMap<>();

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private ProjectLogRepository projectLogRepository;

        @Inject
        private UserProvider userProvider;

        @Inject
        private ConfigurationRepository configRepository;

        private ProjectLogEntity projectLog = new ProjectLogEntity();

        private List<ProjectEntity> allPossibleProjects;

        private ProjectEntity selectedProject;

        private ProjectEntity transferedProject;

        private User user;

        private Configuration config;

        private Integer budget;

        private DualListModel<ProjectEntity> projectDualList;

        @PostConstruct
        private void init() {
                this.user = userProvider.getUser();
                this.allPossibleProjects = projectRepository.findAllOrdered();
                mapForConverter.putAll(allPossibleProjects.stream().collect(Collectors.toMap(ProjectEntity::getId, project -> project)));
                projectsForAYear.addAll(allPossibleProjects.stream().filter(p -> p.getWorkflowState() == State.running.getValue())
                                .collect(Collectors.toList()));
                availableProjects
                                .addAll(allPossibleProjects.stream()
                                                .filter(p -> p.getWorkflowState() == State.requested.getValue()
                                                                || p.getWorkflowState() == State.putaside.getValue())
                                                .collect(Collectors.toList()));
                this.config = configRepository.loadConfiguration();
                List<ProjectEntity> projectsSource = this.availableProjects;
                List<ProjectEntity> projectsTarget = this.projectsForAYear;
                this.projectLog.setEditorId(user);
                projectLog.setTimestampInsert(new Date());

                projectDualList = new DualListModel<ProjectEntity>(projectsSource, projectsTarget);

                calculateUsedBudget(projectsForAYear);
        }

        private void calculateUsedBudget(List<ProjectEntity> runningProjects) {
                this.budget = config.getBudgetForYear();
                Iterator<ProjectEntity> projectsItr = runningProjects.iterator();
                while (projectsItr.hasNext()) {
                        ProjectEntity project = projectsItr.next();
                        this.budget = this.budget - project.getBudget();
                }
        }

        public void onTransfer(TransferEvent event) {
                StringBuilder builder = new StringBuilder();
                for (Object item : event.getItems()) {
                        this.transferedProject = ((ProjectEntity) item);
                        builder.append(transferedProject.getName()).append("<br />");

                        if (event.isAdd()) {
                                addProjectToPriorityList(transferedProject);
                        } else if (event.isRemove()) {
                                removeProjectFromAvailiableList(transferedProject);
                        }
                }
                Collections.sort(availableProjects, new ProjectComparator());
                Collections.sort(projectsForAYear, new ProjectComparator());
                this.projectDualList = new DualListModel<ProjectEntity>(availableProjects, projectsForAYear);
                RequestContext.getCurrentInstance().update("PojoPickList");

        }

        private void removeProjectFromAvailiableList(ProjectEntity projectToRemove) {
                this.budget = this.budget + projectToRemove.getBudget();
                Integer id = projectToRemove.getId();
                if (workflowStates.containsKey(id)) {
                        projectToRemove.setWorkflowState(workflowStates.get(id));
                        workflowStates.remove(id);
                } else {
                        projectToRemove.setWorkflowState(State.putaside.getValue());
                        putasideProjects.put(projectToRemove.getId(), projectToRemove);
                }
                projectsForAYear.remove(projectToRemove);
                availableProjects.add(projectToRemove);
        }

        public void savePriorityList() {
                for (ProjectEntity project : projectsForAYear) {
                        projectRepository.updateProject(project);
                        if (workflowStates.containsKey(project.getId())) {
                                projectLog.setProjectId(project.getId());
                                projectLog.setState(project.getWorkflowState());
                                projectLogRepository.addProjectLog(projectLog);
                        }
                }
                for (ProjectEntity project : availableProjects) {
                        projectRepository.updateProject(project);
                        if (putasideProjects.containsKey(project.getId())) {
                                projectLog.setProjectId(project.getId());
                                projectLog.setState(project.getWorkflowState());
                                projectLogRepository.addProjectLog(projectLog);
                        }
                }
                configRepository.saveConfiguration(config);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Erfolg", "Projekte wurden erfolgreich ver�ndert!"));
        }

        private void addProjectToPriorityList(ProjectEntity projectToAdd) {
                if ((this.budget - projectToAdd.getBudget()) >= 0) {
                        if (putasideProjects.containsKey(projectToAdd.getId())) {
                                putasideProjects.remove(projectToAdd.getId());
                        } else {
                                workflowStates.put(projectToAdd.getId(), projectToAdd.getWorkflowState());
                        }
                        projectToAdd.setWorkflowState(2);
                        projectsForAYear.add(projectToAdd);
                        availableProjects.remove(projectToAdd);
                        this.budget = this.budget - projectToAdd.getBudget();
                } else {
                        RequestContext.getCurrentInstance().execute("PF('noBudgetVar').show();");
                }
        }

        public void raiseBudget() {
                Integer budgetVariance = this.budget - this.transferedProject.getBudget();
                this.budget = 0;
                config.setBudgetForYear(config.getBudgetForYear() - budgetVariance);
                workflowStates.put(transferedProject.getId(), transferedProject.getWorkflowState());
                this.transferedProject.setWorkflowState(2);
                RequestContext.getCurrentInstance().execute("PF('noBudgetVar').hide();");
                projectsForAYear.add(transferedProject);
                availableProjects.remove(transferedProject);
                Collections.sort(availableProjects, new ProjectComparator());
                Collections.sort(projectsForAYear, new ProjectComparator());
                this.projectDualList = new DualListModel<ProjectEntity>(availableProjects, projectsForAYear);
                RequestContext.getCurrentInstance().update("form");
        }

        public void recommendedPriority() {
                Iterator<ProjectEntity> projectsItr = availableProjects.iterator();
                Integer restBudget = this.budget;
                List<ProjectEntity> removeFromAvailable = new LinkedList<>();
                while (projectsItr.hasNext()) {
                        ProjectEntity project = projectsItr.next();
                        if (project.getBudget() <= restBudget) {
                                if (!projectsForAYear.contains(project)) {
                                        projectsForAYear.add(project);
                                }
                                removeFromAvailable.add(project);
                                restBudget = restBudget - project.getBudget();
                        }
                }
                availableProjects.removeAll(removeFromAvailable);
                Collections.sort(availableProjects, new ProjectComparator());
                Collections.sort(projectsForAYear, new ProjectComparator());
                this.projectDualList = new DualListModel<ProjectEntity>(availableProjects, projectsForAYear);
        }

        public void noRaiseBudget() {
                RequestContext.getCurrentInstance().execute("PF('noBudgetVar').hide();");
        }

        public Urgency[] getPrio() {
                return Urgency.values();
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public State[] getState() {
                return State.values();
        }

        public List<ProjectEntity> getAvailableProjects() {
                return availableProjects;
        }

        public ProjectEntity getSelectedProject() {
                return selectedProject;
        }

        public void setSelectedProject(ProjectEntity selectedProject) {
                this.selectedProject = selectedProject;
        }

        public DualListModel<ProjectEntity> getProjectDualList() {
                return projectDualList;
        }

        public void setProjectDualList(DualListModel<ProjectEntity> projectDualList) {
                this.projectDualList = projectDualList;
        }

}