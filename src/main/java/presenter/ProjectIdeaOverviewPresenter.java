package presenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectIdeaEntity;
import enums.Extent;
import enums.Role;
import enums.State;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectIdeaRepository;
import util.UserProvider;

/**
 * @author pascal.euhus
 *
 *         Dieser Presenter ist f�r die projectidea-overview zust�ndig. Je nach
 *         Berechtigung werden die Daten f�r die Tabelle aus der DB geholt.
 *
 */
@Named
@ViewScoped
public class ProjectIdeaOverviewPresenter implements Serializable {

        private static final long serialVersionUID = -2937498972800281877L;

        @Inject
        private ProjectIdeaRepository ideaRepository;

        @Inject
        private LocationRepository locationRepository;

        @Inject
        private DepartmentRepository departmentRepository;

        @Inject
        private UserProvider userProvider;

        private List<ProjectIdeaEntity> projectIdeas = new ArrayList<>();

        private List<LocationEntity> locations = new ArrayList<>();

        private List<DepartmentEntity> departments = new ArrayList<>();

        private ProjectIdeaEntity selectedProjectIdea = new ProjectIdeaEntity();

        private Boolean disabled = true;

        private User user;

        private List<Boolean> showColumn;

        @PostConstruct
        public void init() {
                this.user = userProvider.getUser();
                this.projectIdeas = loadTableData(user.getRole());
                this.locations = locationRepository.findAll();
                this.departments = departmentRepository.findAll();
                this.showColumn = Arrays.asList(false, true, true, false, true, false, false, false, true, true, true);
        }

        private List<ProjectIdeaEntity> loadTableData(Role role) {
                switch (role) {
                case LEADER:
                        return ideaRepository.findAllByDepartmentAndCreator(user);
                case EMPLOYEE:
                        return ideaRepository.findAllByCreator(user.getUserId());
                case MANAGER:
                        return ideaRepository.findAll();
                }
                return null;
        }

        public void onRowClickSelect(final SelectEvent event) {
                setDisabled(false);
        }

        public void onToggle(ToggleEvent e) {
                showColumn.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public State[] getState() {
                return State.values();
        }

        public ProjectIdeaEntity getSelectedProjectIdea() {
                return selectedProjectIdea;
        }

        public void setSelectedProjectIdea(ProjectIdeaEntity selectedProjectIdea) {
                this.selectedProjectIdea = selectedProjectIdea;
        }

        public List<ProjectIdeaEntity> getProjectIdeas() {
                return projectIdeas;
        }

        public void setProjectIdeas(List<ProjectIdeaEntity> projectIdeas) {
                this.projectIdeas = projectIdeas;
        }

        public List<LocationEntity> getLocations() {
                return locations;
        }

        public void setLocations(List<LocationEntity> locations) {
                this.locations = locations;
        }

        public List<DepartmentEntity> getDepartments() {
                return departments;
        }

        public void setDepartments(List<DepartmentEntity> departments) {
                this.departments = departments;
        }

        public List<Boolean> getShowColumn() {
                return showColumn;
        }

        public Boolean getDisabled() {
                return disabled;
        }

        public void setDisabled(Boolean disabled) {
                this.disabled = disabled;
        }

}
