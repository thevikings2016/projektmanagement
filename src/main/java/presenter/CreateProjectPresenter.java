package presenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import domain.ProjectIdeaEntity;
import domain.ProjectLogEntity;
import enums.Extent;
import enums.State;
import enums.Urgency;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectIdeaRepository;
import repository.ProjectLogRepository;
import repository.ProjectRepository;
import repository.UserRepository;
import util.EmptyObjectProvider;
import util.ProjectRater;
import util.UserProvider;

/**
 * @author pascal.euhus
 * 
 *         Presenter f�r das Anlegen eines Projekts, wird per Parameter id eine
 *         ID einer Projektidee mitgegeben, werden die Daten in die Ansicht
 *         geladen und die Idee kann in ein Projekt gewandelt werden.
 *
 */
@Named
@ViewScoped
public class CreateProjectPresenter implements Serializable {

        private static final long serialVersionUID = -6428588069079905235L;

        public static final Logger LOGGER = LoggerFactory.getLogger(CreateProjectPresenter.class);

        private ProjectEntity project = EmptyObjectProvider.getEmptyInstance(ProjectEntity.class);

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private ProjectLogRepository projectLogRepository;

        @Inject
        private UserRepository userRepository;

        @Inject
        private LocationRepository locationRepository;

        @Inject
        private DepartmentRepository departmentRepository;

        @Inject
        private UserProvider userProvider;

        @Inject
        private ProjectIdeaRepository projectideaRepository;

        @Inject
        private ProjectRater projectRater;

        private ProjectLogEntity projectLog = new ProjectLogEntity();

        private List<User> users = new ArrayList<>();

        private List<LocationEntity> locations = new ArrayList<>();

        private List<DepartmentEntity> costUnits = new ArrayList<>();

        private User user;

        private ProjectIdeaEntity projectIdea;

        @PostConstruct
        public void init() {
                this.user = userProvider.getUser();
                this.users = userRepository.findAll();
                this.locations = locationRepository.findAll();
                this.costUnits = departmentRepository.findAll();
                String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
                if (param != null) {
                        try {
                                createProjectfromIdea(param);
                        } catch (NumberFormatException e) {
                                // Do Nothing load Page without Data
                        }
                }
        }

        public Urgency[] getPrio() {
                return Urgency.values();
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public State[] getState() {
                return State.values();
        }

        public void saveProjectAndWriteLog() {
                Integer projectRating = projectRater.getProjectRating(project, projectRater.getConfig());
                project.setRating(projectRating);
                if (projectIdea != null) {
                        projectideaRepository.deleteIdea(projectIdea);
                }
                projectRepository.saveNewProject(project);
                projectLog.setEditorId(user);
                projectLog.setProjectId(project.getId());
                projectLog.setState(project.getWorkflowState());
                projectLog.setTimestampInsert(new Date());
                projectLogRepository.addProjectLog(projectLog);
        }

        public void saveProject() {
                saveProjectAndWriteLog();
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage("growl", new FacesMessage("Erfolg", "Projekt wurde erfolgreich angelegt!"));
        }

        public List<LocationEntity> autocompleteLocations(String city) {
                List<LocationEntity> list = locationRepository.findbyName(city);
                return list;
        }

        public List<String> autocompleteCostUnit(String fullname) {
                List<String> list = costUnits.stream().map(loc -> loc.getFullname())
                                .filter(loc -> loc.toLowerCase().contains(fullname.toLowerCase())).collect(Collectors.toList());
                return list;
        }

        public List<User> autocompleteUser(String username) {
                return userRepository.findbyName(username);
        }

        public void createProjectfromIdea(String ideaId) {
                int ideaID = Integer.parseInt(ideaId);
                this.projectIdea = projectideaRepository.findIdeaByID(ideaID);
                this.project.setName(projectIdea.getName());
                this.project.setCostUnit(projectIdea.getDepartment());
                this.project.setLocation(projectIdea.getLocation());
                this.project.setCurrentSituation(projectIdea.getCurrentSituation());
                this.project.setObjective(projectIdea.getObjective());
                this.project.setBenefit(projectIdea.getBenefit());
        }

        public ProjectEntity getProject() {
                return project;
        }

        public void setProject(ProjectEntity project) {
                this.project = project;
        }

        public User getUser() {
                return user;
        }

        protected void setUser(User user) {
                this.user = user;
        }

        public List<User> getUsers() {
                return users;
        }

        public void setUsers(List<User> users) {
                this.users = users;
        }

        public void setLocations(List<LocationEntity> locations) {
                this.locations = locations;
        }

        public List<LocationEntity> getLocations() {
                return locations;
        }

        public List<DepartmentEntity> getCostUnits() {
                return costUnits;
        }

        public void setCostUnits(List<DepartmentEntity> costUnits) {
                this.costUnits = costUnits;
        }

        protected void setProjectLogRepository(ProjectLogRepository projectLogRepository) {
                this.projectLogRepository = projectLogRepository;
        }

        protected void setProjectRepository(ProjectRepository projectRepository) {
                this.projectRepository = projectRepository;
        }

        protected void setProjectideaRepository(ProjectIdeaRepository projectideaRepository) {
                this.projectideaRepository = projectideaRepository;
        }

        protected void setProjectIdea(ProjectIdeaEntity projectIdea) {
                this.projectIdea = projectIdea;
        }

        protected void setProjectRater(ProjectRater projectRater) {
                this.projectRater = projectRater;
        }
}
