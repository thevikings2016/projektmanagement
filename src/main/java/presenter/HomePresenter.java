package presenter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;

import domain.LocationEntity;
import domain.ProjectEntity;
import enums.State;
import enums.Urgency;
import repository.LocationRepository;
import repository.ProjectRepository;

/**
 * @author pascal.euhus
 * 
 *         Dieser Presenter ist f�r die home View. Hier werden die Daten f�r das
 *         Dashboard geladen und die Kuchendiagramme erzeugt.
 *
 */
@Named
@ViewScoped
public class HomePresenter implements Serializable {

        private static final long serialVersionUID = 3909347629207692510L;

        // Only 250 Projects should run a year
        private final Integer limitProjectsPerYear = 250;

        private PieChartModel pieChartModelCapacity;
        private PieChartModel pieChartModelState;
        private PieChartModel pieChartModelRisk;
        private PieChartModel pieChartModelLocation;

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private LocationRepository locationRepository;

        @PostConstruct
        private void init() {

                // Load the List of all Projects one time for all PieCharts
                List<ProjectEntity> allProjects = projectRepository.findAll();
                createPieModelCapacity(allProjects);
                createPieModelState(allProjects);
                createPieModelRisk(allProjects);
                createPieModelLocation(allProjects);
        }

        private void createPieModelCapacity(List<ProjectEntity> allProjects) {

                pieChartModelCapacity = new PieChartModel();

                // Only 250 Projects should run a year
                // Using a Stream to get a count of all Projects which are
                // "running"
                Integer countProjects = (int) allProjects.parallelStream().filter(p -> p.getWorkflowState() == State.running.getValue())
                                .count();
                Integer freeCapacities = limitProjectsPerYear - countProjects;

                // There are no Capacities if there are 250 or more projects
                // running
                if (freeCapacities < 0) {
                        freeCapacities = 0;
                }

                pieChartModelCapacity.getData().put("Laufende Projekte", countProjects);
                pieChartModelCapacity.getData().put("Freie Kapazit�ten", freeCapacities);

                pieChartModelCapacity.setLegendPosition("ne");
                pieChartModelCapacity.setDiameter(150);
                pieChartModelCapacity.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        }

        private void createPieModelRisk(List<ProjectEntity> allProjects) {

                pieChartModelRisk = new PieChartModel();

                // Using a Stream to get a Map to have all risks with their
                // count of running projects
                Map<Integer, Long> countRisk = allProjects.parallelStream().filter(p -> p.getWorkflowState() == State.running.getValue())
                                .collect(Collectors.groupingBy(p -> p.getRisk(), Collectors.counting()));

                // If a Risk enum isn't used in the DB this Risk enum wouldn't
                // be in the Map
                for (Urgency priority : Urgency.values()) {
                        if (!countRisk.containsKey(priority.getValue())) {
                                countRisk.put(priority.getValue(), 0L);
                        }
                }

                pieChartModelRisk.set(Urgency.veryLow.getName(), countRisk.get(Urgency.veryLow.getValue()));
                pieChartModelRisk.set(Urgency.low.getName(), countRisk.get(Urgency.low.getValue()));
                pieChartModelRisk.set(Urgency.middle.getName(), countRisk.get(Urgency.middle.getValue()));
                pieChartModelRisk.set(Urgency.high.getName(), countRisk.get(Urgency.high.getValue()));
                pieChartModelRisk.set(Urgency.veryHigh.getName(), countRisk.get(Urgency.veryHigh.getValue()));

                pieChartModelRisk.setLegendPosition("ne");
                pieChartModelRisk.setDiameter(150);
                pieChartModelRisk.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        }

        private void createPieModelState(List<ProjectEntity> allProjects) {

                pieChartModelState = new PieChartModel();

                // Using a Stream to get a Map to have all states with their
                // count
                Map<Integer, Long> countState = allProjects.parallelStream()
                                .collect(Collectors.groupingBy(p -> p.getWorkflowState(), Collectors.counting()));

                // If a State isn't used in the DB this State wouldn't be in the
                // Map
                for (State state : State.values()) {
                        if (!countState.containsKey(state.getValue())) {
                                countState.put(state.getValue(), 0L);
                        }
                }

                pieChartModelState.set(State.rejected.getName(), countState.get(State.rejected.getValue()));
                pieChartModelState.set(State.requested.getName(), countState.get(State.requested.getValue()));
                pieChartModelState.set(State.running.getName(), countState.get(State.running.getValue()));
                pieChartModelState.set(State.done.getName(), countState.get(State.done.getValue()));
                pieChartModelState.set(State.putaside.getName(), countState.get(State.putaside.getValue()));
                pieChartModelState.set(State.interupted.getName(), countState.get(State.interupted.getValue()));

                pieChartModelState.setLegendPosition("ne");
                pieChartModelState.setDiameter(150);
                pieChartModelState.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        }

        private void createPieModelLocation(List<ProjectEntity> allProjects) {

                pieChartModelLocation = new PieChartModel();

                // Using a Stream to get a Map to have all risks with their
                // count
                Map<Object, Long> countLocation = allProjects.parallelStream()
                                .collect(Collectors.groupingBy(p -> p.getLocation(), Collectors.counting()));

                // Generate a List of all Locations to iterate over it
                List<LocationEntity> locations = locationRepository.findAll();

                for (LocationEntity location : locations) {
                        // If a Location isn't used in the DB this Location
                        // wouldn't
                        // be in the Map
                        if (!countLocation.containsKey(location)) {
                                countLocation.put(location, 0L);
                        }

                        // Show count of running projects on each location into
                        // the Chart
                        pieChartModelLocation.set(location.getCity(), countLocation.get(location));
                        pieChartModelLocation.setDiameter(150);
                }

                pieChartModelLocation.setLegendPosition("ne");
                pieChartModelLocation.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        }

        public Boolean renderSettings() {
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                StringBuffer requestURL = ((HttpServletRequest) context.getRequest()).getRequestURL();
                if (requestURL.toString().contains("admin") || requestURL.toString().contains("idea")
                                || requestURL.toString().contains("Idea")) {
                        return false;
                } else {
                        return true;
                }
        }

        public PieChartModel getPieChartModelCapacity() {
                return pieChartModelCapacity;
        }

        public PieChartModel getPieChartModelState() {
                return pieChartModelState;
        }

        public PieChartModel getPieChartModelRisk() {
                return pieChartModelRisk;
        }

        public PieChartModel getPieChartModelLocation() {
                return pieChartModelLocation;
        }

}
