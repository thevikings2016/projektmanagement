package presenter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectIdeaEntity;
import enums.Extent;
import enums.State;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectIdeaRepository;
import util.EmptyObjectProvider;
import util.UserProvider;

/**
 * @author pascal.euhus
 * 
 *         Presenter f�r das Anlegen einer Projektidee
 *
 */
@Named
@ViewScoped
public class CreateIdeaPresenter implements Serializable {

        private static final long serialVersionUID = -7659283758528915919L;

        public static final Logger LOGGER = LoggerFactory.getLogger(CreateIdeaPresenter.class);

        private ProjectIdeaEntity projectIdea = EmptyObjectProvider.getEmptyInstance(ProjectIdeaEntity.class);

        @Inject
        private ProjectIdeaRepository ideaRepository;

        @Inject
        private UserProvider userProvider;

        @Inject
        LocationRepository locationRepository;

        @Inject
        DepartmentRepository departmentRepository;

        private User user;

        private List<LocationEntity> locationList;

        private List<DepartmentEntity> costUnitList;

        @PostConstruct
        public void init() {
                this.user = userProvider.getUser();
                setLocationList(locationRepository.findAll());
                setCostUnitList(departmentRepository.findAll());
        }

        public void saveIdea() {
                projectIdea.setWorkflowState(State.requested.getValue());
                projectIdea.setCreator(user);
                projectIdea.setCreatedate(new Date());
                ideaRepository.saveIdea(projectIdea);
                LOGGER.info("Saved Idea with ID {}", projectIdea.getId());
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage("growlCreateIdea", new FacesMessage("Erfolg", "Idee wurde erfolgreich angelegt!"));
        }

        public Extent[] getExtentValues() {
                return Extent.values();
        }

        public ProjectIdeaEntity getProjectIdea() {
                return projectIdea;
        }

        public void setProjectIdea(ProjectIdeaEntity projectIdea) {
                this.projectIdea = projectIdea;
        }

        public List<LocationEntity> getLocationList() {
                return locationList;
        }

        public void setLocationList(List<LocationEntity> locationList) {
                this.locationList = locationList;
        }

        public List<DepartmentEntity> getCostUnitList() {
                return costUnitList;
        }

        public void setCostUnitList(List<DepartmentEntity> costUnitList) {
                this.costUnitList = costUnitList;
        }

}
