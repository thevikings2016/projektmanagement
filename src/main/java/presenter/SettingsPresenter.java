package presenter;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;

import domain.Configuration;
import util.ProjectRater;

/**
 * @author pascal.euhus
 *
 *         Dieser Presenter ist f�r die settings View hier k�nnen die
 *         Application Einstellungen vorgenommen werden. Bei �nderungen werden
 *         alle Projekte neu Bewertet.
 *
 */
@Named
@ViewScoped
public class SettingsPresenter implements Serializable {

        private static final long serialVersionUID = 8387178203241740264L;

        @Inject
        private ProjectRater projectRater;

        private Configuration config;

        @PostConstruct
        private void init() {
                this.config = projectRater.getConfig();
        }

        public Configuration getcurrentConfiguration() {
                return config;
        }

        public void saveConfiguration() {
                projectRater.saveNewConfiguration(config);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Gespeichert!",
                                "Die neuen Bewertungskriterien wurden erfolgreich abgespeichert und die Daten aktualisiert!"));
        }

        public void onTabChange(TabChangeEvent event) {
                String tabID = event.getTab().getId();
                if (tabID.equals("overview")) {
                        RequestContext.getCurrentInstance().update("settingsForm:settingsTab");
                }
        }

}
