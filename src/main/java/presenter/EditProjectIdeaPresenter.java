package presenter;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectIdeaEntity;
import enums.Extent;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectIdeaRepository;

/**
 * @author pascal.euhus
 * 
 *         Presenter f�r die Ansicht change-project-idea.xhtml Es wird auf den
 *         URL Parameter id gepr�ft, wenn der nicht null ist erfolgt ein DB
 *         Zugriff und die Idee wird geladen. Wird zus�tzlich ein action
 *         Parameter definiert wird der EditorTab in der Ansicht geladen. Im
 *         Fehlerfall beim Parsen der ID wird auf die Tabelle zur�ckverlinkt
 *
 */
@Named
@ViewScoped
public class EditProjectIdeaPresenter implements Serializable {

        private static final long serialVersionUID = -8392082610258895335L;

        @Inject
        private ProjectIdeaRepository projectIdeaRepository;

        @Inject
        private LocationRepository locationRepository;

        @Inject
        private DepartmentRepository departmentRepository;

        private List<LocationEntity> locationList;

        private List<DepartmentEntity> costUnitList;

        private Integer projectIdeaId;

        private ProjectIdeaEntity editableProjectIdea;

        private Integer activeTabIndex = 0;

        @PostConstruct
        public void init() {
                setLocationList(locationRepository.findAll());
                setCostUnitList(departmentRepository.findAll());
                String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
                String paramAction = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("action");
                if (param != null) {
                        try {
                                projectIdeaId = Integer.parseInt(param);
                                editableProjectIdea = projectIdeaRepository.findIdeaByID(projectIdeaId);
                                if (paramAction != null && paramAction.equals("edit")) {
                                        activeTabIndex = 1;
                                }
                        } catch (NumberFormatException e) {
                                try {
                                        FacesContext.getCurrentInstance().getExternalContext().redirect("projectidea-overview.jsf");
                                } catch (IOException e1) {
                                        // Do nothing
                                        e1.printStackTrace();
                                }
                        }
                } else {
                        try {
                                FacesContext.getCurrentInstance().getExternalContext().redirect("create-idea.jsf");
                        } catch (IOException e) {
                                // Do nothing
                                e.printStackTrace();
                        }
                }
        }

        public void updateIdea() {
                projectIdeaRepository.updateIdea(editableProjectIdea);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage("growlEditIdea", new FacesMessage("Erfolg", "Daten wurden erfolgreich gespeichert!"));
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public ProjectIdeaEntity getEditableProjectIdea() {
                return editableProjectIdea;
        }

        public void setEditableProjectIdea(ProjectIdeaEntity editableProjectIdea) {
                this.editableProjectIdea = editableProjectIdea;
        }

        public Integer getActiveTabIndex() {
                return activeTabIndex;
        }

        public void setActiveTabIndex(Integer activeTabIndex) {
                this.activeTabIndex = activeTabIndex;
        }

        public LocationRepository getLocationRepository() {
                return locationRepository;
        }

        public void setLocationRepository(LocationRepository locationRepository) {
                this.locationRepository = locationRepository;
        }

        public DepartmentRepository getDepartmentRepository() {
                return departmentRepository;
        }

        public void setDepartmentRepository(DepartmentRepository departmentRepository) {
                this.departmentRepository = departmentRepository;
        }

        public List<LocationEntity> getLocationList() {
                return locationList;
        }

        public void setLocationList(List<LocationEntity> locationList) {
                this.locationList = locationList;
        }

        public List<DepartmentEntity> getCostUnitList() {
                return costUnitList;
        }

        public void setCostUnitList(List<DepartmentEntity> costUnitList) {
                this.costUnitList = costUnitList;
        }

}
