package presenter;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import domain.ProjectLogEntity;
import enums.Extent;
import enums.State;
import enums.Urgency;
import repository.DepartmentRepository;
import repository.LocationRepository;
import repository.ProjectLogRepository;
import repository.ProjectRepository;
import repository.UserRepository;
import util.UserProvider;

/**
 * @author pascal.euhus
 * 
 *         Presenter f�r die Ansicht change-project.xhtml Es wird auf den URL
 *         Parameter id gepr�ft, wenn der nicht null ist erfolgt ein DB Zugriff
 *         und die Idee wird geladen. Wird zus�tzlich ein action Parameter
 *         definiert wird der EditorTab in der Ansicht geladen. Im Fehlerfall
 *         beim Parsen der ID wird auf die Tabelle zur�ckverlinkt
 *
 */
@Named
@ViewScoped
public class EditProjectPresenter implements Serializable {

        private static final long serialVersionUID = -511730446054490298L;

        @Inject
        private ProjectRepository projectRepository;

        @Inject
        private ProjectLogRepository projectLogRepository;

        @Inject
        private UserProvider userProvider;

        @Inject
        private UserRepository userRepository;

        @Inject
        private DepartmentRepository departmentRepository;

        @Inject
        private LocationRepository locationRepository;

        private List<User> users = new ArrayList<>();

        private List<LocationEntity> locations = new ArrayList<>();

        private List<DepartmentEntity> costUnits = new ArrayList<>();

        private List<ProjectLogEntity> projectLogs = new ArrayList<>();

        private User user;

        private Integer projectId;

        private ProjectEntity editableProject;

        private Integer activeTabIndex = 0;

        private State workflowState;

        private Integer originWorkflowState;

        @PostConstruct
        public void init() {
                this.user = userProvider.getUser();
                this.setUsers(userRepository.findAll());
                this.setLocations(locationRepository.findAll());
                this.setCostUnits(departmentRepository.findAll());
                String paramId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
                String paramAction = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("action");
                if (paramId != null) {
                        try {
                                projectId = Integer.parseInt(paramId);
                                editableProject = projectRepository.findProjectByID(projectId);
                                this.originWorkflowState = editableProject.getWorkflowState();
                                this.setProjectLogs(projectLogRepository.findByProjId(projectId));
                                if (paramAction != null && paramAction.equals("edit")) {
                                        activeTabIndex = 1;
                                }
                        } catch (NumberFormatException e) {
                                try {
                                        FacesContext.getCurrentInstance().getExternalContext().redirect("project-overview.jsf");
                                } catch (IOException e1) {
                                        // Do nothing
                                        e1.printStackTrace();
                                }
                        }
                } else {
                        try {
                                FacesContext.getCurrentInstance().getExternalContext().redirect("create-project.jsf");
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }

        }

        public void updateProject() {
                projectRepository.updateProject(editableProject);
                if (originWorkflowState != editableProject.getWorkflowState()) {
                        ProjectLogEntity log = new ProjectLogEntity();
                        log.setEditorId(user);
                        log.setProjectId(editableProject.getId());
                        log.setState(editableProject.getWorkflowState());
                        log.setTimestampInsert(new Date());
                        projectLogRepository.addProjectLog(log);
                }
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage("growlEdit", new FacesMessage("Erfolg", "Daten wurden erfolgreich gespeichert!"));
        }

        public List<LocationEntity> autocompleteLocations(String city) {
                List<LocationEntity> list = locationRepository.findbyName(city);
                return list;
        }

        public List<String> autocompleteCostUnit(String fullname) {
                List<String> list = costUnits.stream().map(loc -> loc.getFullname())
                                .filter(loc -> loc.toLowerCase().contains(fullname.toLowerCase())).collect(Collectors.toList());
                return list;
        }

        public List<User> autocompleteUser(String username) {
                return userRepository.findbyName(username);
        }

        // returns appropriate css class based on integer representation
        public String getBubbleId(int value) {
                switch (value) {
                default:
                        return "veryLow-bubble";
                case 0:
                        return "veryLow-bubble";
                case 1:
                        return "low-bubble";
                case 2:
                        return "mid-bubble";
                case 3:
                        return "high-bubble";
                case 4:
                        return "veryHigh-bubble";
                }
        }

        public Urgency[] getPrio() {
                return Urgency.values();
        }

        public Extent[] getExtent() {
                return Extent.values();
        }

        public State[] getState() {
                return State.values();
        }

        public ProjectEntity getEditableProject() {
                return editableProject;
        }

        public void setEditableProject(ProjectEntity editableProject) {
                this.editableProject = editableProject;
        }

        public Integer getActiveTabIndex() {
                return activeTabIndex;
        }

        public void setActiveTabIndex(Integer activeTabIndex) {
                this.activeTabIndex = activeTabIndex;
        }

        public List<User> getUsers() {
                return users;
        }

        public void setUsers(List<User> users) {
                this.users = users;
        }

        public List<LocationEntity> getLocations() {
                return locations;
        }

        public void setLocations(List<LocationEntity> locations) {
                this.locations = locations;
        }

        public List<DepartmentEntity> getCostUnits() {
                return costUnits;
        }

        public void setCostUnits(List<DepartmentEntity> costUnits) {
                this.costUnits = costUnits;
        }

        public State getWorkflowState() {
                return workflowState;
        }

        public void setWorkflowState(State workflowState) {
                this.workflowState = workflowState;
        }

        public List<ProjectLogEntity> getProjectLogs() {
                return projectLogs;
        }

        public void setProjectLogs(List<ProjectLogEntity> projectLogs) {
                this.projectLogs = projectLogs;
        }

}
