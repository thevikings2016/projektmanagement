package domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import config.User;

@Entity
@Table(name = "project_log")
public class ProjectLogEntity {

        @Id
        private Integer id;

        @Column(name = "proj_id")
        private Integer projectId;

        @ManyToOne
        @JoinColumn(name = "editor")
        private User editorId;

        private Integer state;

        @Column(name = "tsinsert")
        private Date timestampInsert;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getProjectId() {
                return projectId;
        }

        public void setProjectId(Integer projectId) {
                this.projectId = projectId;
        }

        public User getEditorId() {
                return editorId;
        }

        public void setEditorId(User editorId) {
                this.editorId = editorId;
        }

        public Integer getState() {
                return state;
        }

        public void setState(Integer state) {
                this.state = state;
        }

        public Date getTimestampInsert() {
                return timestampInsert;
        }

        public void setTimestampInsert(Date timestampInsert) {
                this.timestampInsert = timestampInsert;
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((editorId == null) ? 0 : editorId.hashCode());
                result = prime * result + ((id == null) ? 0 : id.hashCode());
                result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
                result = prime * result + ((state == null) ? 0 : state.hashCode());
                result = prime * result + ((timestampInsert == null) ? 0 : timestampInsert.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                ProjectLogEntity other = (ProjectLogEntity) obj;
                if (editorId == null) {
                        if (other.editorId != null)
                                return false;
                } else if (!editorId.equals(other.editorId))
                        return false;
                if (id == null) {
                        if (other.id != null)
                                return false;
                } else if (!id.equals(other.id))
                        return false;
                if (projectId == null) {
                        if (other.projectId != null)
                                return false;
                } else if (!projectId.equals(other.projectId))
                        return false;
                if (state == null) {
                        if (other.state != null)
                                return false;
                } else if (!state.equals(other.state))
                        return false;
                if (timestampInsert == null) {
                        if (other.timestampInsert != null)
                                return false;
                } else if (!timestampInsert.equals(other.timestampInsert))
                        return false;
                return true;
        }

        @Override
        public String toString() {
                return "ProjectLogEntity [id=" + id + ", projectId=" + projectId + ", editorId=" + editorId + ", state=" + state
                                + ", timestampInsert=" + timestampInsert + "]";
        }

}
