package domain;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import config.User;
import enums.Extent;
import enums.State;

@Entity
@Table(name = "project_idea")
public class ProjectIdeaEntity implements Serializable {

        private static final long serialVersionUID = -4822777598934371824L;

        public ProjectIdeaEntity() {
                // JPA
        }

        public ProjectIdeaEntity(User creator, DepartmentEntity department, LocationEntity location) {
                this.creator = creator;
                this.department = department;
                this.location = location;
        }

        @Id
        private Integer id;

        private String name;

        @Column(name = "workflow_state")
        private Integer workflowState;

        @ManyToOne
        @JoinColumn(name = "creator")
        private User creator;

        private Date createdate;

        @Column(name = "current_situation")
        private String currentSituation;

        private String objective;

        private String benefit;

        @ManyToOne
        @JoinColumn(name = "cost_unit")
        private DepartmentEntity department;

        @ManyToOne
        @JoinColumn(name = "location")
        private LocationEntity location;

        private Integer extent;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public Integer getWorkflowState() {
                return workflowState;
        }

        public void setWorkflowState(Integer workflowState) {
                this.workflowState = workflowState;
        }

        public State getWorkFlowStateAsEnum(Integer workFlowState) {
                return State.getStateByValue(workFlowState);
        }

        public User getCreator() {
                return creator;
        }

        public void setCreator(User creator) {
                this.creator = creator;
        }

        public Date getCreatedate() {
                return createdate;
        }

        public void setCreatedate(Date createdate) {
                this.createdate = createdate;
        }

        public String getCreateDateAsString() {
                String dateString = null;
                if (createdate != null) {
                        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                        dateString = formatter.format(createdate);
                }
                return dateString;
        }

        public String getCurrentSituation() {
                return currentSituation;
        }

        public void setCurrentSituation(String currentSituation) {
                this.currentSituation = currentSituation;
        }

        public String getObjective() {
                return objective;
        }

        public void setObjective(String objective) {
                this.objective = objective;
        }

        public String getBenefit() {
                return benefit;
        }

        public void setBenefit(String benefit) {
                this.benefit = benefit;
        }

        public DepartmentEntity getDepartment() {
                return department;
        }

        public void setDepartment(DepartmentEntity department) {
                this.department = department;
        }

        public LocationEntity getLocation() {
                return location;
        }

        public void setLocation(LocationEntity location) {
                this.location = location;
        }

        public Integer getExtent() {
                return extent;
        }

        public void setExtent(Integer extent) {
                this.extent = extent;
        }

        public Extent getExtentAsEnum(Integer extent) {
                return Extent.getExtentByValue(extent);
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((benefit == null) ? 0 : benefit.hashCode());
                result = prime * result + ((createdate == null) ? 0 : createdate.hashCode());
                result = prime * result + ((creator == null) ? 0 : creator.hashCode());
                result = prime * result + ((currentSituation == null) ? 0 : currentSituation.hashCode());
                result = prime * result + ((department == null) ? 0 : department.hashCode());
                result = prime * result + ((extent == null) ? 0 : extent.hashCode());
                result = prime * result + ((id == null) ? 0 : id.hashCode());
                result = prime * result + ((location == null) ? 0 : location.hashCode());
                result = prime * result + ((name == null) ? 0 : name.hashCode());
                result = prime * result + ((objective == null) ? 0 : objective.hashCode());
                result = prime * result + ((workflowState == null) ? 0 : workflowState.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                ProjectIdeaEntity other = (ProjectIdeaEntity) obj;
                if (benefit == null) {
                        if (other.benefit != null)
                                return false;
                } else if (!benefit.equals(other.benefit))
                        return false;
                if (createdate == null) {
                        if (other.createdate != null)
                                return false;
                } else if (!createdate.equals(other.createdate))
                        return false;
                if (creator == null) {
                        if (other.creator != null)
                                return false;
                } else if (!creator.equals(other.creator))
                        return false;
                if (currentSituation == null) {
                        if (other.currentSituation != null)
                                return false;
                } else if (!currentSituation.equals(other.currentSituation))
                        return false;
                if (department == null) {
                        if (other.department != null)
                                return false;
                } else if (!department.equals(other.department))
                        return false;
                if (extent == null) {
                        if (other.extent != null)
                                return false;
                } else if (!extent.equals(other.extent))
                        return false;
                if (id == null) {
                        if (other.id != null)
                                return false;
                } else if (!id.equals(other.id))
                        return false;
                if (location == null) {
                        if (other.location != null)
                                return false;
                } else if (!location.equals(other.location))
                        return false;
                if (name == null) {
                        if (other.name != null)
                                return false;
                } else if (!name.equals(other.name))
                        return false;
                if (objective == null) {
                        if (other.objective != null)
                                return false;
                } else if (!objective.equals(other.objective))
                        return false;
                if (workflowState == null) {
                        if (other.workflowState != null)
                                return false;
                } else if (!workflowState.equals(other.workflowState))
                        return false;
                return true;
        }

        @Override
        public String toString() {
                return "ProjectIdeaEntity [id=" + id + ", name=" + name + ", workflowState=" + workflowState + ", creator=" + creator
                                + ", createdate=" + createdate + ", currentSituation=" + currentSituation + ", objective=" + objective
                                + ", benefit=" + benefit + ", department=" + department + ", location=" + location + ", extent=" + extent
                                + "]";
        }

}
