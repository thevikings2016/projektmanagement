package domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import config.User;

@Entity
@Table(name = "department")
public class DepartmentEntity {

        @Id
        private Integer id;

        private String fullname;

        private Integer supervisor;

        @OneToMany(targetEntity = User.class, mappedBy = "department")
        private List<User> users;

        @OneToMany(targetEntity = ProjectIdeaEntity.class, mappedBy = "department")
        private List<ProjectIdeaEntity> projectIdeas;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getFullname() {
                return fullname;
        }

        public void setFullname(String fullname) {
                this.fullname = fullname;
        }

        public Integer getSupervisor() {
                return supervisor;
        }

        public void setSupervisor(Integer supervisor) {
                this.supervisor = supervisor;
        }

        @Override
        public String toString() {
                return "DepartmentEntity [id=" + id + ", name=" + fullname + ", supervisor=" + supervisor + "]";
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((id == null) ? 0 : id.hashCode());
                result = prime * result + ((fullname == null) ? 0 : fullname.hashCode());
                result = prime * result + ((supervisor == null) ? 0 : supervisor.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                DepartmentEntity other = (DepartmentEntity) obj;
                if (id == null) {
                        if (other.id != null)
                                return false;
                } else if (!id.equals(other.id))
                        return false;
                if (fullname == null) {
                        if (other.fullname != null)
                                return false;
                } else if (!fullname.equals(other.fullname))
                        return false;
                if (supervisor == null) {
                        if (other.supervisor != null)
                                return false;
                } else if (!supervisor.equals(other.supervisor))
                        return false;
                return true;
        }

}
