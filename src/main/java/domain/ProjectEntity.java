package domain;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import config.User;
import enums.Extent;
import enums.State;
import enums.Urgency;

@Entity
@Table(name = "project")
public class ProjectEntity implements Serializable {

        private static final long serialVersionUID = -2294392989896714047L;

        public ProjectEntity() {
                // Empty Constructor for JPA
        }

        public ProjectEntity(User customer, User manager, LocationEntity location, DepartmentEntity department) {
                this.location = location;
                this.costUnit = department;
                this.customer = customer;
                this.manager = manager;
        }

        @Id
        private Integer id;

        private String name;

        @ManyToOne
        @JoinColumn(name = "customer")
        private User customer;

        @ManyToOne
        @JoinColumn(name = "manager")
        private User manager;

        @ManyToOne
        @JoinColumn(name = "location")
        private LocationEntity location;

        private Integer extent;

        private Date startdate;

        private Date enddate;

        private Integer progress;

        @Column(name = "workflow_state")
        private Integer workflowState;

        @ManyToOne
        @JoinColumn(name = "cost_unit")
        private DepartmentEntity costUnit;

        private Integer risk;

        @Column(name = "name_recognition_increase")
        private Integer nameRecognitionIncrease;

        @Column(name = "image_gain")
        private Integer imageGain;

        @Column(name = "actual_costs")
        private Integer actualCosts;

        @Column(name = "avg_proceeds_pa")
        private Integer avgProceedsPerYear;

        @Column(name = "use_duration")
        private Integer useDuration;

        @Column(name = "innovation_strategy")
        private Integer innovationStrategy;

        @Column(name = "timing_strategy")
        private Integer timingStrategy;

        @Column(name = "pricing_strategy")
        private Integer pricingStrategy;

        @Column(name = "customer_strategy")
        private Integer customerStrategy;

        private Integer urgency;

        private String objective;

        @Column(name = "current_situation")
        private String currentSituation;

        private String benefit;

        private Integer npv;

        @Column(name = "int_costs")
        private Integer intCosts;

        @Column(name = "ext_costs")
        private Integer extCosts;

        @Transient
        private Period duration;

        private Integer rating;

        private Integer roi;

        @Transient
        private Integer amortizationTime;

        // Returns amortization time in years
        public Integer getAmortizationTime() {
                if (avgProceedsPerYear != null && avgProceedsPerYear > 0) {
                        return getBudget() / avgProceedsPerYear;
                } else {
                        return 0;
                }

        }

        public Period getDuration() {
                // Convert date to LocalDate and get duration between dates
                duration = Period.between(startdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                                enddate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                return duration;
        }

        public Integer getDurationInDays() {
                // Convert date to LocalDate and get duration between dates
                duration = Period.between(startdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                                enddate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                int months = duration.getMonths();
                int years = duration.getYears();
                int days = duration.getDays();
                Integer totalDurationInDays = years * 12 * 30 + months * 30 + days;
                return totalDurationInDays;
        }

        public String getEndDateAsString() {
                String dateString = null;
                if (enddate != null) {
                        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                        dateString = formatter.format(enddate);
                }
                return dateString;
        }

        public String getStartDateAsString() {
                String dateString = null;
                if (startdate != null) {
                        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                        dateString = formatter.format(startdate);
                }
                return dateString;
        }

        public State getWorkFlowStateAsEnum(Integer workFlowState) {
                return State.getStateByValue(workFlowState);
        }

        public Extent getExtentAsEnum(Integer extent) {
                return Extent.getExtentByValue(extent);
        }

        public Urgency getUrgencyAsEnum(Integer urgency) {
                return Urgency.getExtentByValue(urgency);
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public User getCustomer() {
                return customer;
        }

        public void setCustomer(User customer) {
                this.customer = customer;
        }

        public User getManager() {
                return manager;
        }

        public void setManager(User manager) {
                this.manager = manager;
        }

        public LocationEntity getLocation() {
                return location;
        }

        public void setLocation(LocationEntity location) {
                this.location = location;
        }

        public Integer getExtent() {
                return extent;
        }

        public void setExtent(Integer extent) {
                this.extent = extent;
        }

        public Date getStartdate() {
                return startdate;
        }

        public void setStartdate(Date startdate) {
                this.startdate = startdate;
        }

        public Date getEnddate() {
                return enddate;
        }

        public void setEnddate(Date enddate) {
                this.enddate = enddate;
        }

        public Integer getProgress() {
                return progress;
        }

        public void setProgress(Integer progress) {
                this.progress = progress;
        }

        public Integer getBudget() {
                if (intCosts != null && extCosts != null) {
                        return intCosts + extCosts;
                }
                return 0;
        }

        public Integer getWorkflowState() {
                return workflowState;
        }

        public void setWorkflowState(Integer workflowState) {
                this.workflowState = workflowState;
        }

        public DepartmentEntity getCostUnit() {
                return costUnit;
        }

        public void setCostUnit(DepartmentEntity costUnit) {
                this.costUnit = costUnit;
        }

        public Integer getRisk() {
                return risk;
        }

        public void setRisk(Integer risk) {
                this.risk = risk;
        }

        public Integer getNameRecognitionIncrease() {
                return nameRecognitionIncrease;
        }

        public void setNameRecognitionIncrease(Integer nameRecognitionIncrease) {
                this.nameRecognitionIncrease = nameRecognitionIncrease;
        }

        public Integer getImageGain() {
                return imageGain;
        }

        public void setImageGain(Integer imageGain) {
                this.imageGain = imageGain;
        }

        public Integer getActualCosts() {
                return actualCosts;
        }

        public void setActualCosts(Integer actualCosts) {
                this.actualCosts = actualCosts;
        }

        public Integer getAvgProceedsPerYear() {
                return avgProceedsPerYear;
        }

        public void setAvgProceedsPerYear(Integer avgProceedsPerYear) {
                this.avgProceedsPerYear = avgProceedsPerYear;
        }

        public Integer getUseDuration() {
                return useDuration;
        }

        public void setUseDuration(Integer useDuration) {
                this.useDuration = useDuration;
        }

        public Integer getInnovationStrategy() {
                return innovationStrategy;
        }

        public void setInnovationStrategy(Integer innovationStrategy) {
                this.innovationStrategy = innovationStrategy;
        }

        public Integer getTimingStrategy() {
                return timingStrategy;
        }

        public void setTimingStrategy(Integer timingStrategy) {
                this.timingStrategy = timingStrategy;
        }

        public Integer getPricingStrategy() {
                return pricingStrategy;
        }

        public void setPricingStrategy(Integer pricingStrategy) {
                this.pricingStrategy = pricingStrategy;
        }

        public Integer getCustomerStrategy() {
                return customerStrategy;
        }

        public void setCustomerStrategy(Integer customerStrategy) {
                this.customerStrategy = customerStrategy;
        }

        public Integer getUrgency() {
                return urgency;
        }

        public void setUrgency(Integer urgency) {
                this.urgency = urgency;
        }

        public String getObjective() {
                return objective;
        }

        public void setObjective(String objective) {
                this.objective = objective;
        }

        public String getCurrentSituation() {
                return currentSituation;
        }

        public void setCurrentSituation(String currentSituation) {
                this.currentSituation = currentSituation;
        }

        public String getBenefit() {
                return benefit;
        }

        public void setBenefit(String benefit) {
                this.benefit = benefit;
        }

        public Integer getNpv() {
                return npv;
        }

        public void setNpv(Integer npv) {
                this.npv = npv;
        }

        public Integer getIntCosts() {
                return intCosts;
        }

        public void setIntCosts(Integer intCosts) {
                this.intCosts = intCosts;
        }

        public Integer getExtCosts() {
                return extCosts;
        }

        public void setExtCosts(Integer extCosts) {
                this.extCosts = extCosts;
        }

        public Integer getRoi() {
                return roi;
        }

        public void setRoi(Integer roi) {
                this.roi = roi;
        }

        public Integer getRating() {
                return rating;
        }

        public void setRating(Integer rating) {
                this.rating = rating;
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((actualCosts == null) ? 0 : actualCosts.hashCode());
                result = prime * result + ((avgProceedsPerYear == null) ? 0 : avgProceedsPerYear.hashCode());
                result = prime * result + ((benefit == null) ? 0 : benefit.hashCode());
                result = prime * result + ((costUnit == null) ? 0 : costUnit.hashCode());
                result = prime * result + ((currentSituation == null) ? 0 : currentSituation.hashCode());
                result = prime * result + ((customer == null) ? 0 : customer.hashCode());
                result = prime * result + ((customerStrategy == null) ? 0 : customerStrategy.hashCode());
                result = prime * result + ((duration == null) ? 0 : duration.hashCode());
                result = prime * result + ((enddate == null) ? 0 : enddate.hashCode());
                result = prime * result + ((extCosts == null) ? 0 : extCosts.hashCode());
                result = prime * result + ((extent == null) ? 0 : extent.hashCode());
                result = prime * result + ((id == null) ? 0 : id.hashCode());
                result = prime * result + ((imageGain == null) ? 0 : imageGain.hashCode());
                result = prime * result + ((innovationStrategy == null) ? 0 : innovationStrategy.hashCode());
                result = prime * result + ((intCosts == null) ? 0 : intCosts.hashCode());
                result = prime * result + ((location == null) ? 0 : location.hashCode());
                result = prime * result + ((manager == null) ? 0 : manager.hashCode());
                result = prime * result + ((name == null) ? 0 : name.hashCode());
                result = prime * result + ((nameRecognitionIncrease == null) ? 0 : nameRecognitionIncrease.hashCode());
                result = prime * result + ((npv == null) ? 0 : npv.hashCode());
                result = prime * result + ((objective == null) ? 0 : objective.hashCode());
                result = prime * result + ((pricingStrategy == null) ? 0 : pricingStrategy.hashCode());
                result = prime * result + ((progress == null) ? 0 : progress.hashCode());
                result = prime * result + ((risk == null) ? 0 : risk.hashCode());
                result = prime * result + ((startdate == null) ? 0 : startdate.hashCode());
                result = prime * result + ((timingStrategy == null) ? 0 : timingStrategy.hashCode());
                result = prime * result + ((urgency == null) ? 0 : urgency.hashCode());
                result = prime * result + ((useDuration == null) ? 0 : useDuration.hashCode());
                result = prime * result + ((workflowState == null) ? 0 : workflowState.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                ProjectEntity other = (ProjectEntity) obj;
                if (actualCosts == null) {
                        if (other.actualCosts != null)
                                return false;
                } else if (!actualCosts.equals(other.actualCosts))
                        return false;
                if (avgProceedsPerYear == null) {
                        if (other.avgProceedsPerYear != null)
                                return false;
                } else if (!avgProceedsPerYear.equals(other.avgProceedsPerYear))
                        return false;
                if (benefit == null) {
                        if (other.benefit != null)
                                return false;
                } else if (!benefit.equals(other.benefit))
                        return false;
                if (costUnit == null) {
                        if (other.costUnit != null)
                                return false;
                } else if (!costUnit.equals(other.costUnit))
                        return false;
                if (currentSituation == null) {
                        if (other.currentSituation != null)
                                return false;
                } else if (!currentSituation.equals(other.currentSituation))
                        return false;
                if (customer == null) {
                        if (other.customer != null)
                                return false;
                } else if (!customer.equals(other.customer))
                        return false;
                if (customerStrategy == null) {
                        if (other.customerStrategy != null)
                                return false;
                } else if (!customerStrategy.equals(other.customerStrategy))
                        return false;
                if (duration == null) {
                        if (other.duration != null)
                                return false;
                } else if (!duration.equals(other.duration))
                        return false;
                if (enddate == null) {
                        if (other.enddate != null)
                                return false;
                } else if (!enddate.equals(other.enddate))
                        return false;
                if (extCosts == null) {
                        if (other.extCosts != null)
                                return false;
                } else if (!extCosts.equals(other.extCosts))
                        return false;
                if (extent == null) {
                        if (other.extent != null)
                                return false;
                } else if (!extent.equals(other.extent))
                        return false;
                if (id == null) {
                        if (other.id != null)
                                return false;
                } else if (!id.equals(other.id))
                        return false;
                if (imageGain == null) {
                        if (other.imageGain != null)
                                return false;
                } else if (!imageGain.equals(other.imageGain))
                        return false;
                if (innovationStrategy == null) {
                        if (other.innovationStrategy != null)
                                return false;
                } else if (!innovationStrategy.equals(other.innovationStrategy))
                        return false;
                if (intCosts == null) {
                        if (other.intCosts != null)
                                return false;
                } else if (!intCosts.equals(other.intCosts))
                        return false;
                if (location == null) {
                        if (other.location != null)
                                return false;
                } else if (!location.equals(other.location))
                        return false;
                if (manager == null) {
                        if (other.manager != null)
                                return false;
                } else if (!manager.equals(other.manager))
                        return false;
                if (name == null) {
                        if (other.name != null)
                                return false;
                } else if (!name.equals(other.name))
                        return false;
                if (nameRecognitionIncrease == null) {
                        if (other.nameRecognitionIncrease != null)
                                return false;
                } else if (!nameRecognitionIncrease.equals(other.nameRecognitionIncrease))
                        return false;
                if (npv == null) {
                        if (other.npv != null)
                                return false;
                } else if (!npv.equals(other.npv))
                        return false;
                if (objective == null) {
                        if (other.objective != null)
                                return false;
                } else if (!objective.equals(other.objective))
                        return false;
                if (pricingStrategy == null) {
                        if (other.pricingStrategy != null)
                                return false;
                } else if (!pricingStrategy.equals(other.pricingStrategy))
                        return false;
                if (progress == null) {
                        if (other.progress != null)
                                return false;
                } else if (!progress.equals(other.progress))
                        return false;
                if (risk == null) {
                        if (other.risk != null)
                                return false;
                } else if (!risk.equals(other.risk))
                        return false;
                if (startdate == null) {
                        if (other.startdate != null)
                                return false;
                } else if (!startdate.equals(other.startdate))
                        return false;
                if (timingStrategy == null) {
                        if (other.timingStrategy != null)
                                return false;
                } else if (!timingStrategy.equals(other.timingStrategy))
                        return false;
                if (urgency == null) {
                        if (other.urgency != null)
                                return false;
                } else if (!urgency.equals(other.urgency))
                        return false;
                if (useDuration == null) {
                        if (other.useDuration != null)
                                return false;
                } else if (!useDuration.equals(other.useDuration))
                        return false;
                if (workflowState == null) {
                        if (other.workflowState != null)
                                return false;
                } else if (!workflowState.equals(other.workflowState))
                        return false;
                return true;
        }

        @Override
        public String toString() {
                return "ProjectEntity [id=" + id + ", name=" + name + ", customer=" + customer + ", manager=" + manager + ", location="
                                + location + ", extent=" + extent + ", startdate=" + startdate + ", enddate=" + enddate + ", progress="
                                + progress + ", workflowState=" + workflowState + ", costUnit=" + costUnit + ", risk=" + risk
                                + ", nameRecognitionIncrease=" + nameRecognitionIncrease + ", imageGain=" + imageGain + ", actualCosts="
                                + actualCosts + ", avgProceedsPerYear=" + avgProceedsPerYear + ", useDuration=" + useDuration
                                + ", innovationStrategy=" + innovationStrategy + ", timingStrategy=" + timingStrategy + ", pricingStrategy="
                                + pricingStrategy + ", customerStrategy=" + customerStrategy + ", urgency=" + urgency + ", objective="
                                + objective + ", currentSituation=" + currentSituation + ", benefit=" + benefit + ", npv=" + npv
                                + ", intCosts=" + intCosts + ", extCosts=" + extCosts + ", duration=" + duration + "]";
        }

}