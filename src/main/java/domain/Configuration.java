package domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "systemtable")
public class Configuration {

        @Id
        private Integer id;

        private Long projectCapacity;

        private Integer timingStrategy;

        private Integer pricingStrategy;

        private Integer customerStrategy;

        private Integer innovationStrategy;

        private Integer budget;

        private Integer ammortizationTime;

        private Integer npv;

        private Integer roi;

        private Integer duration;

        private Integer risk;

        private Integer budgetForYear;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Long getProjectCapacity() {
                return projectCapacity;
        }

        public void setProjectCapacity(Long projectCapacity) {
                this.projectCapacity = projectCapacity;
        }

        public Integer getTimingStrategy() {
                return timingStrategy;
        }

        public void setTimingStrategy(Integer timingStrategy) {
                this.timingStrategy = timingStrategy;
        }

        public Integer getPricingStrategy() {
                return pricingStrategy;
        }

        public void setPricingStrategy(Integer pricingStrategy) {
                this.pricingStrategy = pricingStrategy;
        }

        public Integer getCustomerStrategy() {
                return customerStrategy;
        }

        public void setCustomerStrategy(Integer customerStrategy) {
                this.customerStrategy = customerStrategy;
        }

        public Integer getInnovationStrategy() {
                return innovationStrategy;
        }

        public void setInnovationStrategy(Integer innovationStrategy) {
                this.innovationStrategy = innovationStrategy;
        }

        public Integer getBudget() {
                return budget;
        }

        public void setBudget(Integer budget) {
                this.budget = budget;
        }

        public Integer getAmmortizationTime() {
                return ammortizationTime;
        }

        public void setAmmortizationTime(Integer ammortizationTime) {
                this.ammortizationTime = ammortizationTime;
        }

        public Integer getNpv() {
                return npv;
        }

        public void setNpv(Integer npv) {
                this.npv = npv;
        }

        public Integer getRoi() {
                return roi;
        }

        public void setRoi(Integer roi) {
                this.roi = roi;
        }

        public Integer getDuration() {
                return duration;
        }

        public void setDuration(Integer duration) {
                this.duration = duration;
        }

        public Integer getRisk() {
                return risk;
        }

        public void setRisk(Integer risk) {
                this.risk = risk;
        }

        public Integer getBudgetForYear() {
                return budgetForYear;
        }

        public void setBudgetForYear(Integer budgetForYear) {
                this.budgetForYear = budgetForYear;
        }

        @Override
        public String toString() {
                return "Configuration [id=" + id + ", projectCapacity=" + projectCapacity + ", timingStrategy=" + timingStrategy
                                + ", pricingStrategy=" + pricingStrategy + ", customerStrategy=" + customerStrategy
                                + ", innovationStrategy=" + innovationStrategy + ", budget=" + budget + ", ammortizationTime="
                                + ammortizationTime + ", npv=" + npv + ", roi=" + roi + ", duration=" + duration + ", risk=" + risk
                                + ", budgetForYear=" + budgetForYear + "]";
        }

        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + ((ammortizationTime == null) ? 0 : ammortizationTime.hashCode());
                result = prime * result + ((budget == null) ? 0 : budget.hashCode());
                result = prime * result + ((budgetForYear == null) ? 0 : budgetForYear.hashCode());
                result = prime * result + ((customerStrategy == null) ? 0 : customerStrategy.hashCode());
                result = prime * result + ((duration == null) ? 0 : duration.hashCode());
                result = prime * result + ((id == null) ? 0 : id.hashCode());
                result = prime * result + ((innovationStrategy == null) ? 0 : innovationStrategy.hashCode());
                result = prime * result + ((npv == null) ? 0 : npv.hashCode());
                result = prime * result + ((pricingStrategy == null) ? 0 : pricingStrategy.hashCode());
                result = prime * result + ((projectCapacity == null) ? 0 : projectCapacity.hashCode());
                result = prime * result + ((risk == null) ? 0 : risk.hashCode());
                result = prime * result + ((roi == null) ? 0 : roi.hashCode());
                result = prime * result + ((timingStrategy == null) ? 0 : timingStrategy.hashCode());
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                Configuration other = (Configuration) obj;
                if (ammortizationTime == null) {
                        if (other.ammortizationTime != null)
                                return false;
                } else if (!ammortizationTime.equals(other.ammortizationTime))
                        return false;
                if (budget == null) {
                        if (other.budget != null)
                                return false;
                } else if (!budget.equals(other.budget))
                        return false;
                if (budgetForYear == null) {
                        if (other.budgetForYear != null)
                                return false;
                } else if (!budgetForYear.equals(other.budgetForYear))
                        return false;
                if (customerStrategy == null) {
                        if (other.customerStrategy != null)
                                return false;
                } else if (!customerStrategy.equals(other.customerStrategy))
                        return false;
                if (duration == null) {
                        if (other.duration != null)
                                return false;
                } else if (!duration.equals(other.duration))
                        return false;
                if (id == null) {
                        if (other.id != null)
                                return false;
                } else if (!id.equals(other.id))
                        return false;
                if (innovationStrategy == null) {
                        if (other.innovationStrategy != null)
                                return false;
                } else if (!innovationStrategy.equals(other.innovationStrategy))
                        return false;
                if (npv == null) {
                        if (other.npv != null)
                                return false;
                } else if (!npv.equals(other.npv))
                        return false;
                if (pricingStrategy == null) {
                        if (other.pricingStrategy != null)
                                return false;
                } else if (!pricingStrategy.equals(other.pricingStrategy))
                        return false;
                if (projectCapacity == null) {
                        if (other.projectCapacity != null)
                                return false;
                } else if (!projectCapacity.equals(other.projectCapacity))
                        return false;
                if (risk == null) {
                        if (other.risk != null)
                                return false;
                } else if (!risk.equals(other.risk))
                        return false;
                if (roi == null) {
                        if (other.roi != null)
                                return false;
                } else if (!roi.equals(other.roi))
                        return false;
                if (timingStrategy == null) {
                        if (other.timingStrategy != null)
                                return false;
                } else if (!timingStrategy.equals(other.timingStrategy))
                        return false;
                return true;
        }

}
