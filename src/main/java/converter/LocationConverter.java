package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import domain.LocationEntity;
import repository.LocationRepository;

/**
 * @author pascal.euhus
 * 
 * 
 *         Converter werden genutzt um die Enum-Werte in lesbare String werte zu
 *         mappen z.B f�r die Dropdowns
 *
 */
@FacesConverter(value = "locationConverter")
public class LocationConverter implements Converter {

        @Inject
        private LocationRepository locationRepository;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
                try {
                        return locationRepository.findById(Integer.parseInt(value));
                } catch (NumberFormatException e) {
                        System.out.println("Not a valid department id: " + value);
                        return null;
                }
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                        System.out.println("Can not convert null to LocationEntity String");
                        return "";
                } else if (value instanceof LocationEntity) {
                        return String.valueOf(((LocationEntity) value).getId());
                } else {
                        System.out.println("Object is no instance of LocationEntity: " + value);
                        return null;
                }
        }
}