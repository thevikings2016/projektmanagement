package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import config.User;
import repository.UserRepository;

/**
 * @author pascal.euhus
 * 
 *         Converter werden genutzt um die Enum-Werte in lesbare String werte zu
 *         mappen z.B f�r die Dropdowns, Autocomplete
 *
 */
@FacesConverter(value = "userConverter")
public class UserConverter implements Converter {

        Logger LOGGER = LoggerFactory.getLogger(UserConverter.class);

        @Inject
        private UserRepository userRepository;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
                LOGGER.debug("Trying to find User by Name {}", value);
                return userRepository.findbyName(value).get(0);
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                        LOGGER.debug("Can not convert {} to User", value);
                        return "";
                } else if (value instanceof User) {
                        return ((User) value).getFullname();
                } else {
                        LOGGER.debug("Object is no instance of User: " + value);
                        return null;
                }
        }

}
