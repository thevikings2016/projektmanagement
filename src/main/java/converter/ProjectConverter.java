package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import domain.ProjectEntity;
import presenter.PriorityOverviewPresenter;

/**
 * @author pascal.euhus
 * 
 *         Converter wird genutzt f�r die Picklist in der Priorisierungs-Ansicht
 *         um die Projekte darstellen zu k�nnen
 *
 */
@FacesConverter(value = "projectConverter")
public class ProjectConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
                try {
                        return PriorityOverviewPresenter.mapForConverter.get(Integer.parseInt(value));
                } catch (NumberFormatException e) {
                        System.out.println("Not a valid project id: " + value);
                        return null;
                }
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                        System.out.println("Can not convert null to ProjectEntity String");
                        return "";
                } else if (value instanceof ProjectEntity) {
                        return String.valueOf(((ProjectEntity) value).getId());
                } else {
                        System.out.println("Object is no instance of ProjectEntity: " + value);
                        return null;
                }
        }
}