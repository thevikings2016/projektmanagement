package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import domain.DepartmentEntity;
import repository.DepartmentRepository;

/**
 * @author pascal.euhus
 * 
 *         Converter werden genutzt um die Enum-Werte in lesbare String werte zu
 *         mappen z.B f�r die Dropdowns
 *
 */
@FacesConverter(value = "departmentConverter")
public class DepartmentConverter implements Converter {

        @Inject
        private DepartmentRepository departmentRepository;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
                try {
                        return departmentRepository.findById(Integer.parseInt(value));
                } catch (NumberFormatException e) {
                        System.out.println("Not a valid department id: " + value);
                        return null;
                }
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                        System.out.println("Can not convert null to DepartmentEntity String");
                        return "";
                } else if (value instanceof DepartmentEntity) {
                        return String.valueOf(((DepartmentEntity) value).getId());
                } else {
                        System.out.println("Object is no instance of DepartmentEntity: " + value);
                        return null;
                }
        }
}