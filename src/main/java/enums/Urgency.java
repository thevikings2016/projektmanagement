package enums;

public enum Urgency {

        veryLow("sehr niedrig", 0), low("niedrig", 1), middle("mittel", 2), high("hoch", 3), veryHigh("sehr hoch", 4);

        private Integer value;
        private String name;

        Urgency(String name, Integer value) {
                this.name = name;
                this.value = value;
        }

        public Integer getValue() {
                return value;
        }

        public String getName() {
                return name;
        }

        public static Urgency getExtentByValue(Integer value) {
                for (Urgency urgency : Urgency.values()) {
                        if (urgency.getValue() == value) {
                                return urgency;
                        }
                }
                return null;
        }
}
