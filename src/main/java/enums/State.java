package enums;

public enum State {

        rejected("abgelehnt", 0), requested("beantragt", 1), running("laufend", 2), done("fertig", 3), putaside("zur�ckgestellt",
                        4), interupted("abgebrochen", 5);

        private Integer value;
        private String name;

        State(String name, Integer value) {
                this.name = name;
                this.value = value;
        }

        public Integer getValue() {
                return value;
        }

        public String getName() {
                return name;
        }

        public static State getStateByValue(Integer value) {
                for (State state : State.values()) {
                        if (state.getValue() == value) {
                                return state;
                        }
                }
                return null;
        }
}
