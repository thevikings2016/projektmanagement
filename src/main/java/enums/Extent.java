package enums;

public enum Extent {

        Abteilungsprojekt(0), Standortprojekt(1), Unternehmensprojekt(2);

        private Integer value;

        Extent(Integer value) {
                this.value = value;
        }

        public Integer getValue() {
                return value;
        }

        public String getShortName() {
                return this.name().replace("sprojekt", "").replace("projekt", "");
        }

        public static Extent getExtentByValue(Integer value) {
                for (Extent extent : Extent.values()) {
                        if (extent.getValue() == value) {
                                return extent;
                        }
                }
                return null;
        }

}
