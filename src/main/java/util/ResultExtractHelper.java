package util;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * @author pascal.euhus
 *
 *         Diese Klasse ist ein Helfer f�r den DB Zugriff, wenn nur ein Result
 *         erwartet wird. Standartm��ig wirft Hibernate eine
 *         {@link NoResultException} wenn kein Ergebnis gefunden wird aber eins
 *         erwartet ist. Um dieses Problem zu Umgehen wird Null zur�ckgegeben
 *         statt einer Exception.
 *
 */
public abstract class ResultExtractHelper {

        public static <T> T getSingleResultOrNull(TypedQuery<T> query) {
                query.setMaxResults(1);
                List<T> resultList = query.getResultList();
                if (resultList.isEmpty()) {
                        return null;
                } else {
                        return resultList.get(0);
                }
        }

}
