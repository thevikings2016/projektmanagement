package util;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import config.LoginBean;
import config.User;

/**
 * @author pascal.euhus
 *
 *         Kapselung um nicht die LoginBean selbst direkt referenzieren zu
 *         m�ssen und Zugriff auschlie�lich auf das User Objekt zu gew�hren.
 *
 */
@SessionScoped
public class UserProvider implements Serializable {

        private static final long serialVersionUID = 210876865048222983L;

        @Inject
        private LoginBean loginBean;

        public User getUser() {
                return loginBean.getUser();
        }

}
