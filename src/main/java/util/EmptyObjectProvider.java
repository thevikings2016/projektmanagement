package util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import config.User;
import domain.DepartmentEntity;
import domain.LocationEntity;
import domain.ProjectEntity;
import domain.ProjectIdeaEntity;

/**
 * @author pascal.euhus
 * 
 *         Hier k�nnen leere Instanzen f�r Projektideen oder Projekte erzeugt
 *         werden, mit der Sicherheit, dass es keine
 *         {@link NullPointerException} durch Verwendung der leeren Objekte gibt
 *
 */
public abstract class EmptyObjectProvider {

        public static <T> T getEmptyInstance(Class<T> clazz) {
                T instance = null;
                Constructor<?> constructor;
                try {
                        Class<?> clazzForName = Class.forName(clazz.getName());
                        if (clazzForName.getName().equals(ProjectEntity.class.getName())) {
                                constructor = clazzForName.getConstructor(User.class, User.class, LocationEntity.class,
                                                DepartmentEntity.class);
                                instance = clazz.cast(constructor.newInstance(
                                                new Object[] { new User(), new User(), new LocationEntity(), new DepartmentEntity() }));
                        } else if (clazzForName.getName().equals(ProjectIdeaEntity.class.getName())) {
                                constructor = clazzForName.getConstructor(User.class, DepartmentEntity.class, LocationEntity.class);
                                instance = clazz.cast(constructor
                                                .newInstance(new Object[] { new User(), new DepartmentEntity(), new LocationEntity() }));
                        }

                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
                                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                        e.printStackTrace();
                }
                return instance;
        }

}
