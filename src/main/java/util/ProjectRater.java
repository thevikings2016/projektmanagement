package util;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import domain.Configuration;
import domain.ProjectEntity;
import repository.ConfigurationRepository;
import repository.ProjectRepository;

/**
 * @author pascal.euhus
 * 
 *         ProjectRater Kapselt die Berechnung der Berwertung eines Projektes,
 *         au�erdem wird die Konfiguration f�r die Bewertung hier verwaltet.
 *
 */
@Stateless
public class ProjectRater {

        @Inject
        private ConfigurationRepository configurationRepository;

        @Inject
        private ProjectRepository projectRepository;

        public void saveNewConfiguration(Configuration config) {
                configurationRepository.saveConfiguration(config);
                List<ProjectEntity> updatetProjects = reEvaluateProjectsRanking();
                updatetProjects.forEach(project -> projectRepository.updateProject(project));
        }

        private List<ProjectEntity> reEvaluateProjectsRanking() {
                List<ProjectEntity> allProjects = projectRepository.findAll();
                Configuration config = getConfig();
                allProjects.forEach(project -> project.setRating(getProjectRating(project, config)));
                return allProjects;
        }

        public Integer getProjectRating(ProjectEntity project, Configuration config) {

                Integer costRating = 0;
                Integer costs = project.getBudget();
                if (costs >= 1000000) {
                        costRating = 1;
                } else if (costs >= 500000 && costs < 1000000) {
                        costRating = 2;
                } else if (costs >= 250000 && costs < 500000) {
                        costRating = 3;
                } else if (costs >= 100000 && costs < 250000) {
                        costRating = 4;
                } else if (costs < 100000) {
                        costRating = 5;
                }

                Integer npvRating = 0;
                Integer npv = project.getNpv();
                if (npv >= 1000000) {
                        npvRating = 5;
                } else if (npv >= 500000 && npv < 1000000) {
                        npvRating = 4;
                } else if (npv >= 250000 && npv < 500000) {
                        npvRating = 3;
                } else if (npv >= 100000 && npv < 250000) {
                        npvRating = 2;
                } else if (npv < 100000) {
                        npvRating = 1;
                }

                Integer durationRating = 0;
                Integer duration = project.getDurationInDays();
                if (duration >= 48 * 30) {
                        durationRating = 1;
                } else if (duration >= 24 * 30 && duration < 48 * 30) {
                        durationRating = 2;
                } else if (duration >= 12 * 30 && duration < 24 * 30) {
                        durationRating = 3;
                } else if (duration >= 6 * 30 && duration < 12 * 30) {
                        durationRating = 4;
                } else if (duration < 6 * 30) {
                        durationRating = 5;
                }

                Integer riskRating = 0;
                Integer risk = project.getRisk();
                switch (risk) {
                case 0:
                        riskRating = 5;
                        break;
                case 1:
                        riskRating = 4;
                        break;
                case 2:
                        riskRating = 3;
                        break;
                case 3:
                        riskRating = 2;
                        break;
                case 4:
                        riskRating = 1;
                        break;
                }

                // TODO Hier fehlt ROI und Amortisationszeit

                Integer rating = (project.getTimingStrategy()) * config.getTimingStrategy()
                                + (project.getPricingStrategy()) * config.getPricingStrategy()
                                + (project.getCustomerStrategy()) * config.getCustomerStrategy()
                                + (project.getInnovationStrategy()) * config.getInnovationStrategy() + (riskRating * config.getRisk())
                                + (costRating * config.getBudget()) + (durationRating * config.getDuration())
                                + (npvRating * config.getNpv());

                return rating;

        }

        public Configuration getConfig() {
                return configurationRepository.loadConfiguration();
        }

}
