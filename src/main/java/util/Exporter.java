package util;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;

/**
 * @author pascal.euhus
 * 
 *         Der Exporter bereitet die PDF Datei vor und setzt das Layout
 *
 */
@Named
@ApplicationScoped
public class Exporter implements Serializable {

        private static final long serialVersionUID = -5345541383950676622L;

        public void preProcessPDF(Object document) {
                Document pdf = (Document) document;
                pdf.setPageSize(PageSize.A4.rotate());
                pdf.open();

                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                String logo = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "images" + File.separator
                                + "supernova-logo-transparent_small.png";

                try {
                        pdf.add(Image.getInstance(logo));
                } catch (DocumentException | IOException e) {
                        e.printStackTrace();
                }
        }
}
